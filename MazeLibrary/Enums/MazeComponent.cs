﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeLibrary.Enums
{
    public enum MazeComponent
    {
        Pass,
        Wall,
        Personage,
        Entry,
        Exit,
        Track,
        TrackLeft,
        TrackRight,
        TrackDown,
        Question
    }
}
