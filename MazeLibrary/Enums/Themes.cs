﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeLibrary.Enums
{
    // Множество тем
    public enum Themes
    {
        Spring,
        Summer,
        Autumn,
        Winter
    }
}
