﻿using MazeLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using NHibernate.Linq.Visitors;

namespace MazeLibrary.MazeClass
{
    public static class FindWayMaze
    {
        // Волновой алгоритм нахождения пути
        public static List<Point> WaveWay(MazeStruct mazeElement, MazeStruct.Update updateMaze, ref int speed)
        {
            var maze = mazeElement.GetMaze(); // получаем исходный лабиринт
            var listWay = new List<Point>(); // список путей

            int[,] mazeWave = new int[maze.GetLength(0), maze.GetLength(1)]; // создаём новый массив для волнового алгоритма

            // заполнение массива
            for (int i = 0; i < maze.GetLength(0); i++)
            {
                for (int j = 0; j < maze.GetLength(1); j++)
                {
                    mazeWave[i, j] = maze[i, j] == MazeComponent.Wall ? -1 : 0; // если у нас есть стена, то ставим -1, туда попасть нельзя.иначе ставим ноль
                }
            }

            // создаём список со списком волн и первым элементом входом
            var listWave = new List<List<Point>>()
            {
                new List<Point>()
                {
                    new Point(mazeElement.GetEntry().X,mazeElement.GetEntry().Y)
                }
            };

            mazeWave[listWave[0][0].Y, listWave[0][0].X] = -1; // на вход устанавливаем -1, туда попасть нельзя

            while (ListWaveFindExit(listWave, maze) == -1) // пока в списке волн ннет выхода
            {
                var currentWave = listWave[listWave.Count - 1]; // получаем последнюю волну из списка волн

                var newWave = new List<Point>(); // список точек новой волны

                currentWave.ForEach((obj) => // проверяем есть ли перходы в точки 
                {
                    if (obj.X - 1 >= 0 && mazeWave[obj.Y, obj.X - 1] == 0) // если внизу стоит ноль
                    {
                        mazeWave[obj.Y, obj.X - 1] = listWave.Count; //устанавливаем индекс волны
                        newWave.Add(new Point(obj.X - 1, obj.Y)); // добавляем в список
                    }

                    if (obj.X + 1 < maze.GetLength(1) && mazeWave[obj.Y, obj.X + 1] == 0) // если справа нет волны
                    {
                        mazeWave[obj.Y, obj.X + 1] = listWave.Count; //устанавливаем индекс волны
                        newWave.Add(new Point(obj.X + 1, obj.Y));
                    }

                    if (obj.Y - 1 >= 0 && mazeWave[obj.Y - 1, obj.X] == 0) // если слева нет волны
                    {
                        mazeWave[obj.Y - 1, obj.X] = listWave.Count;
                        newWave.Add(new Point(obj.X, obj.Y - 1));
                    }

                    if (obj.Y + 1 < maze.GetLength(0) && mazeWave[obj.Y + 1, obj.X] == 0) // если сверху нет волны
                    {
                        mazeWave[obj.Y + 1, obj.X] = listWave.Count;
                        newWave.Add(new Point(obj.X, obj.Y + 1));
                    }
                });

                listWave.Add(newWave); // в список волн добавляем новую волну
            }

            var currentPosition = new Point(mazeElement.GetExit().X, mazeElement.GetExit().Y); // ставим текущую позицию в точку выхода

            if (currentPosition.Y == 0) // если выход сверху
            {
                currentPosition.Y += 1;
            }
            else if (currentPosition.Y == mazeElement.Width - 1) // если выход снизу
            {
                currentPosition.Y -= 1;
            }
            else if (currentPosition.X == 0) // если выход слева
            {
                currentPosition.X += 1;
            }
            else if (currentPosition.X == mazeElement.Length - 1) //если выход справа
            {
                currentPosition.X -= 1;
            }

            listWay.Add(new Point(currentPosition.X, currentPosition.Y)); //добавляем путь

            mazeWave[mazeElement.GetEntry().Y, mazeElement.GetEntry().X] = int.MinValue; //устанавливаем входу минимальное значение

            while (mazeElement.GetMaze()[currentPosition.Y, currentPosition.X] != MazeComponent.Entry) // пока не достигнем выхода
            {
                if (currentPosition.X - 1 >= 0  // если слева номер волны меньше чем текущий
                    && mazeWave[currentPosition.Y, currentPosition.X - 1] <
                    mazeWave[currentPosition.Y, currentPosition.X]
                    && mazeWave[currentPosition.Y, currentPosition.X - 1] != -1 // если там не стена 
                    && mazeWave[currentPosition.Y, currentPosition.X - 1] != 0) // если мы там ещё не были
                {
                    currentPosition.X -= 1; // переходим влево
                    listWay.Add(new Point(currentPosition.X, currentPosition.Y)); //добавляем точку в список
                }
                else if (currentPosition.X + 1 < mazeElement.Length // если справа номер волны меньше чем текущий
                         && mazeWave[currentPosition.Y, currentPosition.X + 1] <
                         mazeWave[currentPosition.Y, currentPosition.X]
                         && mazeWave[currentPosition.Y, currentPosition.X + 1] != -1// если там не стена 
                         && mazeWave[currentPosition.Y, currentPosition.X + 1] != 0)// если мы там ещё не были
                {
                    currentPosition.X += 1; // перходим вправо
                    listWay.Add(new Point(currentPosition.X, currentPosition.Y));
                }
                else if (currentPosition.Y - 1 >= 0 // если сверху номер волны меньше чем текущий
                         && mazeWave[currentPosition.Y - 1, currentPosition.X] <
                         mazeWave[currentPosition.Y, currentPosition.X]
                         && mazeWave[currentPosition.Y - 1, currentPosition.X] != -1// если там не стена 
                         && mazeWave[currentPosition.Y - 1, currentPosition.X] != 0)// если мы там ещё не были
                {
                    currentPosition.Y -= 1; //переходим вверх
                    listWay.Add(new Point(currentPosition.X, currentPosition.Y));
                }
                else if (currentPosition.Y + 1 < mazeElement.Length // если слнизу номер волны меньше чем текущий
                         && mazeWave[currentPosition.Y + 1, currentPosition.X] <
                         mazeWave[currentPosition.Y, currentPosition.X]
                         && mazeWave[currentPosition.Y + 1, currentPosition.X] != -1// если там не стена 
                         && mazeWave[currentPosition.Y + 1, currentPosition.X] != 0)// если мы там ещё не были
                {
                    currentPosition.Y += 1; //перходим вниз
                    listWay.Add(new Point(currentPosition.X, currentPosition.Y));
                }
            }

            listWay.Reverse(); // Производим инверсию списку

            foreach (var el in listWay)
            {
                mazeElement.SetMovePersonage(el.X, el.Y); // перемещаем персонажа
                updateMaze(mazeElement.GetMaze(), ref speed);
            }

            return listWay; // возращаем список путей
        }

        // проверяем есть ли выход в списке списков путей
        private static int ListWaveFindExit(List<List<Point>> listWave, MazeComponent[,] maze)
        {
            var index = 0;

            while (index < listWave.Count) // 
            {
                // проверка есть ли в списке списков путей ячейка с выходом
                if (listWave[index].Where(obj => maze[obj.Y, obj.X] == MazeComponent.Exit).ToList().Count != 0)
                    break;

                index++;// перход к следующему списку
            }


            return index == listWave.Count ? -1 : index; // если не нашли, то -1
        }

        // множество направления персонажа
        enum Direction
        {
            Top, // верх
            Bot, // низ
            Left, // лево
            Right //право
        }
        public static void OneWayHand(MazeStruct mazeElement, MazeStruct.Update updateMaze, ref int speed)
        {
            var entry = mazeElement.GetEntry(); //получаем вход

            var currentPosition = new Point(entry.X, entry.Y); // устанавливаем точку на вход

            var maze = mazeElement.GetMaze(); // получаем лабиринт

            var currentDirection = Direction.Top; //предустанавливаем вход

            // в зависимоти от входу устанавливаем направление
            if (entry.X == 0) // если вход слева
                currentDirection = Direction.Right;
            if (entry.X == mazeElement.Length - 1) // если вход справа
                currentDirection = Direction.Left;
            if (entry.Y == 0) // если вход вверху
                currentDirection = Direction.Bot;
            if (entry.Y == mazeElement.Width - 1) // если вход внизу
                currentDirection = Direction.Top;


            while (maze[currentPosition.Y, currentPosition.X] != MazeComponent.Exit) // Пока не достигнем выхода
            {
                // right
                // проверяем можем ли мы туда пойти. Есть ли снизу стена, чтобы идти вправо.
                while (
                    currentPosition.Y + 1 < mazeElement.Width
                    && currentDirection == Direction.Right
                    && currentPosition.X + 1 < mazeElement.Length
                    && (maze[currentPosition.Y, currentPosition.X + 1] != MazeComponent.Wall
                    || maze[currentPosition.Y + 1, currentPosition.X] == MazeComponent.Track)
                    && (maze[currentPosition.Y + 1, currentPosition.X] == MazeComponent.Wall
                    || maze[currentPosition.Y + 1, currentPosition.X] == MazeComponent.Entry)
                    && maze[currentPosition.Y, currentPosition.X] != MazeComponent.Exit
                )
                {
                    currentPosition.X += 1;// преходим влево

                    mazeElement.SetMovePersonage(currentPosition.X, currentPosition.Y);
                    updateMaze(maze, ref speed); //отображение пути
                }

                // bot
                // проверяем можем ли мы туда пойти. Есть ли справа стена, чтобы идти вниз.
                while (
                    currentPosition.X - 1 >= 0
                    && currentDirection == Direction.Bot
                    && currentPosition.Y + 1 < mazeElement.Width
                    && (maze[currentPosition.Y + 1, currentPosition.X] != MazeComponent.Wall
                        || maze[currentPosition.Y, currentPosition.X - 1] == MazeComponent.Track)
                    && (maze[currentPosition.Y, currentPosition.X - 1] == MazeComponent.Wall
                    || maze[currentPosition.Y, currentPosition.X - 1] == MazeComponent.Entry)
                    && maze[currentPosition.Y, currentPosition.X] != MazeComponent.Exit
                )
                {
                    currentPosition.Y += 1; // перходим вниз

                    mazeElement.SetMovePersonage(currentPosition.X, currentPosition.Y);
                    updateMaze(maze, ref speed); // отображаем перемещение
                }

                // left
                // проверяем можем ли мы туда пойти. Есть ли сверху стена, чтобы идти влево.
                while (
                    currentPosition.X - 1 >= 0
                    && currentDirection == Direction.Left
                    && currentPosition.Y - 1 < mazeElement.Width
                    && (maze[currentPosition.Y, currentPosition.X - 1] != MazeComponent.Wall
                        || maze[currentPosition.Y - 1, currentPosition.X] == MazeComponent.Track)
                    && (maze[currentPosition.Y - 1, currentPosition.X] == MazeComponent.Wall
                    || maze[currentPosition.Y - 1, currentPosition.X] == MazeComponent.Entry)
                    && maze[currentPosition.Y, currentPosition.X] != MazeComponent.Exit
                )
                {
                    currentPosition.X -= 1; // переходим влево

                    mazeElement.SetMovePersonage(currentPosition.X, currentPosition.Y);
                    updateMaze(maze, ref speed); // отображаем перемещение
                }

                // top 
                // проверяем можем ли мы туда пойти. Есть ли снизу справа, чтобы идти вверх.
                while (
                    currentPosition.X + 1 < mazeElement.Length
                    && currentDirection == Direction.Top
                    && currentPosition.Y - 1 >= 0
                    && (maze[currentPosition.Y - 1, currentPosition.X] != MazeComponent.Wall
                    || maze[currentPosition.Y, currentPosition.X + 1] == MazeComponent.Track)
                    && (maze[currentPosition.Y, currentPosition.X + 1] == MazeComponent.Wall
                    || maze[currentPosition.Y, currentPosition.X + 1] == MazeComponent.Entry)
                    && maze[currentPosition.Y, currentPosition.X] != MazeComponent.Exit
                )
                {
                    currentPosition.Y -= 1; // перемещаемся вверх

                    mazeElement.SetMovePersonage(currentPosition.X, currentPosition.Y);
                    updateMaze(maze, ref speed); // отображаем перемещение
                }

                switch (currentDirection)
                {
                    case Direction.Top: // если текущее направление вверх и мы упёрлист в стену
                        currentDirection = Direction.Right; // меняем направление вправо

                        if (currentPosition.X + 1 < mazeElement.Length // если справа стена, то меняем направление
                            && (maze[currentPosition.Y, currentPosition.X + 1] != MazeComponent.Wall
                                && maze[currentPosition.Y, currentPosition.X + 1] != MazeComponent.Entry))
                        {
                            currentPosition.X += 1; // перезодим влево

                            mazeElement.SetMovePersonage(currentPosition.X, currentPosition.Y);
                            updateMaze(maze, ref speed);
                        }
                        else if (currentPosition.X - 1 >= 0
                                 && (maze[currentPosition.Y, currentPosition.X - 1] != MazeComponent.Wall
                                     && maze[currentPosition.Y, currentPosition.X - 1] != MazeComponent.Entry))
                        {
                            currentDirection = Direction.Left; // меняем направление влево

                            currentPosition.X -= 1; // переходим влево

                            mazeElement.SetMovePersonage(currentPosition.X, currentPosition.Y);
                            updateMaze(maze, ref speed);
                        }


                        break;

                    case Direction.Bot: // если текущее направление вниз и мы упёрлись в стену
                        currentDirection = Direction.Left;

                        if (currentPosition.X - 1 >= 0 //если слева не стена, то идём влево
                            && (maze[currentPosition.Y, currentPosition.X - 1] != MazeComponent.Wall
                                && maze[currentPosition.Y, currentPosition.X - 1] != MazeComponent.Entry))
                        {
                            currentPosition.X -= 1;

                            mazeElement.SetMovePersonage(currentPosition.X, currentPosition.Y);
                            updateMaze(maze, ref speed);
                        }
                        else if (currentPosition.X + 1 < mazeElement.Length // если слева стена, меняем направление
                                 && (maze[currentPosition.Y, currentPosition.X + 1] != MazeComponent.Wall
                                     && maze[currentPosition.Y, currentPosition.X + 1] != MazeComponent.Entry))
                        {
                            currentDirection = Direction.Right;

                            currentPosition.X += 1;

                            mazeElement.SetMovePersonage(currentPosition.X, currentPosition.Y);
                            updateMaze(maze, ref speed);
                        }


                        break;

                    case Direction.Left: // если текущее направление влево и мы упёрлись в стену
                        currentDirection = Direction.Top;

                        if (currentPosition.Y - 1 >= 0 //если сверху нет стены
                            && (maze[currentPosition.Y - 1, currentPosition.X] != MazeComponent.Wall
                                && maze[currentPosition.Y - 1, currentPosition.X] != MazeComponent.Entry))
                        {
                            currentPosition.Y -= 1;

                            mazeElement.SetMovePersonage(currentPosition.X, currentPosition.Y);
                            updateMaze(maze, ref speed);
                        }
                        else if (currentPosition.Y + 1 < mazeElement.Width //если снизу нет стены, то идём вниз
                                 && (maze[currentPosition.Y + 1, currentPosition.X] != MazeComponent.Wall
                                     && maze[currentPosition.Y + 1, currentPosition.X] != MazeComponent.Entry))
                        {
                            currentDirection = Direction.Bot;

                            currentPosition.Y += 1;

                            mazeElement.SetMovePersonage(currentPosition.X, currentPosition.Y);
                            updateMaze(maze, ref speed);
                        }

                        break;

                    case Direction.Right: // если текущее направление вправо и мы упёрлись в стену
                        currentDirection = Direction.Bot;

                        if (currentPosition.Y + 1 < mazeElement.Width // если снизу нет стены
                            && (maze[currentPosition.Y + 1, currentPosition.X] != MazeComponent.Wall
                                && maze[currentPosition.Y + 1, currentPosition.X] != MazeComponent.Entry))
                        {
                            currentPosition.Y += 1;

                            mazeElement.SetMovePersonage(currentPosition.X, currentPosition.Y);
                            updateMaze(maze, ref speed);
                        }
                        else if (currentPosition.Y - 1 >= 0 // если снизу стена, и сверху нет стены то меняем направление наверх
                                 && (maze[currentPosition.Y - 1, currentPosition.X] != MazeComponent.Wall
                                     && maze[currentPosition.Y - 1, currentPosition.X] != MazeComponent.Entry))
                        {
                            currentDirection = Direction.Top;

                            currentPosition.Y -= 1;

                            mazeElement.SetMovePersonage(currentPosition.X, currentPosition.Y);
                            updateMaze(maze, ref speed);
                        }


                        break;
                }


            }
        }
    }
}
