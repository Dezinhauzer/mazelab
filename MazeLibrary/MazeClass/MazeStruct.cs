﻿using MazeLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using MazeLibrary.MazeClass;
using Remotion.Linq.Parsing;
using MazeLibrary.Database;
using System.Diagnostics;

namespace MazeLibrary.MazeClass
{
    public class MazeStruct
    {
        private long _id; // идентификатор лабиринта
        private int _length; //  длина лабиринта
        private int _width; // высота лабиринта
        private Themes _theme; // тема лабиринта

        public delegate void Update(MazeComponent[,] maze, ref int speed); // делагат отображения изменения

        private MazeComponent[,] _mazeComponents; // Матрица лабиринта

        private Point Exit { get; set; } // гет и сет выход
        private Point Entry { get; set; } // геттер и сеттер взода
        private Point Personage { get; set; } // геттер и сеттер персонажа


        public MazeComponent[,] GetMaze() => _mazeComponents; // геттер лабиринта

        // пустой конструктор
        public MazeStruct()
        {

        }

        // контсрутор лабиринта с темой, высотой и шириной
        public MazeStruct(int length, int width, Themes theme)
        {
            _length = length; // устанавливаем ширину
            _width = width; // устанавливаем высоту
            _theme = theme; // тему
            _mazeComponents = new MazeComponent[_width, _length]; //создаём массив компонентов лабиринта
            GenerateWallEntryExit(); // генирируем стену по краям
        }

        // генерация стены по краям
        private void GenerateWallEntryExit()
        {
            for (int i = 0; i < _width; i++) // устанавливаем стены слвеа и справа
            {
                _mazeComponents[i, 0] = _mazeComponents[i, _length - 1] = MazeComponent.Wall;
            }

            for (int j = 0; j < _length; j++) // устанавливаем стены сверху и снизу
            {
                _mazeComponents[_width - 1, j] = _mazeComponents[0, j] = MazeComponent.Wall;
            }
        }

        private bool IsBorder(int x, int y) // если гявляется граинцей, то возращаем истину
        {
            return x == 0 || x == _length - 1
                || y == 0 || y == _width - 1;

        }

        private bool IsAngle(int x, int y) // проверяем является ли точка углом
        {
            return (x == 0 || x == _length - 1) && (y == 0 || y == _width - 1);
        }

        private bool CheckComponent(int x, int y, MazeComponent mazeComponent) // проверяем если ли компонента сверху, снизу, слева, справа
        {
            return (x + 1 < _length && _mazeComponents[y, x + 1] == mazeComponent)
                || (x - 1 >= 0 && _mazeComponents[y, x - 1] == mazeComponent)
                || (y + 1 < _width && _mazeComponents[y + 1, x] == mazeComponent)
                || (y - 1 >= 0 && _mazeComponents[y - 1, x] == mazeComponent);
        }

        private bool CheckComponentDiagonal(int x, int y, MazeComponent component) // проверяем есть ли компонент в диагоналях:сверху слева и справа или снизу слева и справа
        {
            return (x - 1 >= 0 && y - 1 >= 0 && _mazeComponents[y - 1, x - 1] == component)
                || (x - 1 >= 0 && y + 1 < _width && _mazeComponents[y + 1, x - 1] == component)
                || (x + 1 < _length && y + 1 < _width && _mazeComponents[y + 1, x + 1] == component)
                || (x + 1 < _length && y - 1 >= 0 && _mazeComponents[y - 1, x + 1] == component);

        }

        public void SetPointWall(int x, int y) // устанавливаем стену в точке
        {
            _mazeComponents[y, x] = MazeComponent.Wall;
        }

        public void SetQuestion()
        {
            for (int i = 1; i < _width - 1; i += 2) // устанавливаем стены слвеа и справа
            {
                if (!((Entry.X == 0 && Entry.Y == i)
                    || (Exit.X == 0 && Exit.Y == i))
                    )
                    _mazeComponents[i, 0] = MazeComponent.Question;

                if (!((Entry.X == _length - 1 && Entry.Y == i)
                    || (Exit.X == _length - 1 && Exit.Y == i))
                    )
                    _mazeComponents[i, _length - 1] = MazeComponent.Question;
            }

            for (int j = 1; j < _length - 1; j += 2) // устанавливаем стены сверху и снизу
            {
                if (!((Entry.X == j && Entry.Y == _width - 1)
                    || (Exit.X == j && Exit.Y == _width - 1)))
                    _mazeComponents[_width - 1, j] = MazeComponent.Question;

                if (!((Entry.X == j && Entry.Y == 0)
                    || (Exit.X == j && Exit.Y == 0)))
                    _mazeComponents[0, j] = MazeComponent.Question;
            }

            SetWallDiagonalWidth(Entry);
            SetWallDiagonalWidth(Exit);
            SetWallDiagonalLength(Entry);
            SetWallDiagonalLength(Exit);
        }

        private void SetWallDiagonalLength(Point point)
        {
            if (point.Y == 0)
            {
                if (point.X == 1)
                {
                    _mazeComponents[1, 0] = MazeComponent.Wall;
                }

                if (point.X == _length - 2)
                {
                    _mazeComponents[1, _length - 1] = MazeComponent.Wall;
                }
            }

            if (point.Y == _width - 2)
            {
                if (point.X == 1)
                {
                    _mazeComponents[_width - 2, 0] = MazeComponent.Wall;
                }

                if (point.X == _length - 2)
                {
                    _mazeComponents[_length - 2, _width - 1] = MazeComponent.Wall;
                }
            }
        }

        public void SetWallWithExitEntry()
        {
            for (int i = 1; i < _width - 1; i += 2) // устанавливаем стены слвеа и справа
            {
                if (!((Entry.X == 0 && Entry.Y == i)
                    || (Exit.X == 0 && Exit.Y == i))
                    )
                    _mazeComponents[i, 0] = MazeComponent.Wall;

                if (!((Entry.X == _length - 1 && Entry.Y == i)
                    || (Exit.X == _length - 1 && Exit.Y == i))
                    )
                    _mazeComponents[i, _length - 1] = MazeComponent.Wall;
            }

            for (int j = 1; j < _length - 1; j += 2) // устанавливаем стены сверху и снизу
            {
                if (!((Entry.X == j && Entry.Y == _width - 1)
                    || (Exit.X == j && Exit.Y == _width - 1)))
                    _mazeComponents[_width - 1, j] = MazeComponent.Wall;

                if (!((Entry.X == j && Entry.Y == 0)
                    || (Exit.X == j && Exit.Y == 0)))
                    _mazeComponents[0, j] = MazeComponent.Wall;
            }
        }

        private void SetWallDiagonalWidth(Point point)
        {
            if (point.X == 0)
            {
                if (point.Y == 1)
                {
                    _mazeComponents[0, 1] = MazeComponent.Wall;
                }

                if (point.Y == _width - 2)
                {
                    _mazeComponents[_width - 1, 1] = MazeComponent.Wall;
                }
            }

            if (point.X == _length - 1)
            {
                if (point.Y == 1)
                {
                    _mazeComponents[0, _length - 2] = MazeComponent.Wall;
                }

                if (point.Y == _width - 2)
                {
                    _mazeComponents[_width - 1, _length - 2] = MazeComponent.Wall;
                }
            }
        }

        public void SetPointPass(int x, int y) // устанавливаем проход
        {
            if (!IsBorder(x, y)) // если не является границей, то устанавливаем проход
            {
                _mazeComponents[y, x] = MazeComponent.Pass;
            }

        }

        public void SetPointExit(int x, int y) // устанавливаем выход
        {
            if (IsBorder(x, y) // выход устанавливается если он не рядом со входом, если на границе и если он на чётной позиции
                && !IsAngle(x, y)
                && !CheckComponent(x, y, MazeComponent.Entry)
                && !CheckComponentDiagonal(x, y, MazeComponent.Entry)
                && !CheckMod(x, y)
                && !NotPoint(new Point(x, y), Entry))
            {
                _mazeComponents[Exit.Y, Exit.X] = MazeComponent.Wall;
                _mazeComponents[y, x] = MazeComponent.Exit;
                Exit = new Point(x, y);
            }
        }

        private bool CheckMod(int x, int y) // проверяем чётность позиции
        {
            if (x == 0 || x == _length - 1)
                return y % 2 == 0;

            if (y == 0 || y == _width - 1)
                return x % 2 == 0;

            return true;
        }

        public void SetPointEntry(int x, int y) // устнавливаем вход
        {
            if (IsBorder(x, y) // вход устанавливается если он не рядом со выходом, если на границе и если он на чётной позиции
                && !IsAngle(x, y)
                && !CheckComponent(x, y, MazeComponent.Exit)
                && !CheckComponentDiagonal(x, y, MazeComponent.Exit)
                && !CheckMod(x, y)
                && !NotPoint(new Point(x, y), Exit))
            {
                _mazeComponents[Entry.Y, Entry.X] = MazeComponent.Wall;
                _mazeComponents[y, x] = MazeComponent.Entry;
                Entry = new Point(x, y);

                // если персонажа нет, то создаём
                if (Personage != new Point())
                    _mazeComponents[Personage.Y, Personage.X] = MazeComponent.Pass;

                // в зависимости от того где персонаж устанавливаем его на первую клетку от входа
                if (x == 0)
                    Personage = new Point(x + 1, y);
                else if (x == Length - 1)
                    Personage = new Point(x - 1, y);
                else if (y == 0)
                    Personage = new Point(x, y + 1);
                else if (y == Width - 1)
                    Personage = new Point(x, y - 1);

                _mazeComponents[Personage.Y, Personage.X] = MazeComponent.Personage;
            }
        }

        private bool NotPoint(Point point1, Point point2) // Равенство точек
        {
            return point1.X == point2.X && point1.Y == point2.Y;
        }

        public MazeStruct(MazeEntity mazeEntity) // преобразование сущности из бд в объект
        {
            _id = mazeEntity.Id;
            _length = mazeEntity.Length;
            _width = mazeEntity.Width;
            _theme = mazeEntity.Theme;
            _mazeComponents = StringToArray(mazeEntity.ArrayMaze, _length, _width); // получаем лабиринт

            for (int i = 0; i < _width; i++) // ищим входы и выходы
            {
                for (int j = 0; j < _length; j++)
                {
                    if (_mazeComponents[i, j] == MazeComponent.Entry)
                        SetPointEntry(j, i);
                    if (_mazeComponents[i, j] == MazeComponent.Exit)
                        SetPointExit(j, i);
                }
            }

        }


        public MazeEntity GetEntity() // получаем объект базы данных
        {
            var maze = new MazeEntity
            {
                Id = _id,
                Length = _length,
                Width = _width,
                Theme = _theme,
                ArrayMaze = ArrayToString(_mazeComponents) // преобразуем в массив строк
            };

            return maze;
        }

        private readonly Dictionary<string, MazeComponent> _dicMazeComponents = new Dictionary<string, MazeComponent>() // словарь слов и компонентов
        {
            {"Entry", MazeComponent.Entry },
            {"Exit", MazeComponent.Exit },
            {"Pass", MazeComponent.Pass },
            {"Personage", MazeComponent.Personage },
            {"Track", MazeComponent.Track },
            {"Wall", MazeComponent.Wall }
        };

        // геттеры и сеттеры
        public long Id { get => _id; set => _id = value; }
        public int Length { get => _length; set => _length = value; }
        public int Width { get => _width; set => _width = value; }
        public Themes Theme { get => _theme; set => _theme = value; }

        private MazeComponent[,] StringToArray(string str, int length, int widht) // преобразование из строки в массив
        {
            var newArray = new MazeComponent[widht, length];
            var arr = str.Split(','); // получаем все компоненты в виде строк


            for (int i = 0; i < widht; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    newArray[i, j] = _dicMazeComponents[arr[i * length + j]]; // новый элемент, строки берём из словаря
                }

            }

            return newArray; // возвращаем новый массив компонентов
        }

        private static string ArrayToString(MazeComponent[,] mazeComponents) // проебразуем в массив
        {
            var str = "";
            for (int i = 0; i < mazeComponents.GetLength(0); i++)
            {
                for (int j = 0; j < mazeComponents.GetLength(1); j++)
                {
                    str += mazeComponents[i, j].ToString() + ','; // преобразуем в строку
                }
            }

            return str.Substring(0, str.Length - 1); // удаляем последнуюю запятую
        }

        public Point GetPersonage() => Personage;

        public Point GetEntry()
        {
            return Entry;
        }

        public Point GetExit() => Exit;

        // установка перемещениеяперсонажа
        public bool SetMovePersonage(int newX, int newY)
        {
            if (_mazeComponents[newY, newX] != MazeComponent.Wall // если не стена не выход и не вход, то можно перейти
                && _mazeComponents[newY, newX] != MazeComponent.Exit
                && _mazeComponents[newY, newX] != MazeComponent.Entry)
            {
                _mazeComponents[Personage.Y, Personage.X] = _mazeComponents[newY, newX] == MazeComponent.Track ||
                    _mazeComponents[newY, newX] == MazeComponent.TrackLeft ||
                    _mazeComponents[newY, newX] == MazeComponent.TrackRight ||
                    _mazeComponents[newY, newX] == MazeComponent.TrackDown ? MazeComponent.Pass
                    : SetTrack(Personage.Y,Personage.X,newX,newY); 
                ; // устанавливаем след на предыдущую позицию
                _mazeComponents[newY, newX] = MazeComponent.Personage;
                Personage = new Point(newX, newY);
            }

            return _mazeComponents[newY, newX] == MazeComponent.Exit ? true : false; // если достигли конца, то возращаем тру

        }

        private MazeComponent SetTrack(int y, int x, int newX, int newY)
        {
            if (y == newY)
            {
                if (newX > x)
                    return MazeComponent.TrackRight;
                else
                    return MazeComponent.TrackLeft;
            }
            else
            {
                if (!(newY > y))
                    return MazeComponent.Track;
                else
                    return MazeComponent.TrackDown;
            }
        }

        public void AutoSetEntryExit() // автоматическая установка входов и выходов
        {
            if (Exit != new Point()) // если выход не пуст
            {
                _mazeComponents[Exit.Y, Exit.X] = MazeComponent.Wall;
            }

            if (Entry != new Point()) // если вход не пуст
            {
                _mazeComponents[Entry.Y, Entry.X] = MazeComponent.Wall;
                _mazeComponents[Personage.Y, Personage.X] = MazeComponent.Pass;
            }

            var exitEntry = new int[] { _width, _length };

            Random random = new Random();

            // выбираем стороны на которой генерируются входы выходы
            var elementExitEntry = exitEntry[random.Next(0, 2)];

            Point entryPoint, exitPoint, personagePoint;

            // Находим координаты входов и выходов
            if (elementExitEntry == _length)
            {
                entryPoint = new Point(GetIndex(1, _length - 1, random), 0);
                personagePoint = new Point(entryPoint.X, entryPoint.Y + 1);
                exitPoint = new Point(GetIndex(1, _length - 1, random), _width - 1);
            }
            else
            {
                entryPoint = new Point(0, GetIndex(1, _width - 1, random));
                personagePoint = new Point(entryPoint.X + 1, entryPoint.Y);
                exitPoint = new Point(_length - 1, GetIndex(1, _width - 1, random));
            }

            // Устанавливаем вход и выход на массиве
            _mazeComponents[entryPoint.Y, entryPoint.X] = MazeComponent.Entry;
            _mazeComponents[exitPoint.Y, exitPoint.X] = MazeComponent.Exit;
            _mazeComponents[personagePoint.Y, personagePoint.X] = MazeComponent.Personage;

            // устанавливаем вход выход и персонажа так
            Exit = exitPoint;
            Entry = entryPoint;
            Personage = personagePoint;
        }

        private int GetIndex(int left, int right, Random random) // получаем индекс входа массива
        {
            int index = 0;
            do
            {
                index = random.Next(left, right);
            } while (index % 2 == 0);

            return index;
        }

        public bool IsSetExitEntry() // если вход и вход устанвлены
        {
            return Entry != new Point() && Exit != new Point();
        }

        public void GeneratePrim(Update updateMazeComponent, ref int speed) // Алгоритм генерации прима
        {
            _mazeComponents = GenerateMaze.GenerateMazePrim(this, updateMazeComponent, ref speed);
        }

        public void GenerateKruskala(Update updateMazeComponent, ref int speed) // алгоритм генерации Краскала
        {
            _mazeComponents = GenerateMaze.GenerateMazeKruskala(this, updateMazeComponent, ref speed);
        }

        public List<Point> ExcelentStruct() // Проверка структуры лабиринта
        {
            var errorListPoints = new List<Point>(); // список ошибок
            for (int i = 1; i < Width - 1; i++)
            {
                for (int j = 1; j < Length - 1; j++)
                {
                    if (CheckElementSet(j, i))
                    {
                        errorListPoints.Add(new Point(j, i)); // добавляем элемент
                    }
                }
            }

            FindCycle(errorListPoints);

            return errorListPoints; // возвращаем список ошибок
        }

        private void FindCycle(List<Point> errorListPoints)
        {
            var mazeWay = new int[_mazeComponents.GetLength(0), _mazeComponents.GetLength(1)];

            for (int i = 0; i < mazeWay.GetLength(0); i++)
            {
                for (int j = 0; j < mazeWay.GetLength(1); j++)
                {
                    mazeWay[i, j] = _mazeComponents[i, j] == MazeComponent.Pass
                        || _mazeComponents[i, j] == MazeComponent.Personage ? 0 : -1;

                }
            }

            int index = 1;

            for (int i = 1; i < mazeWay.GetLength(0) - 1; i++)
            {
                for (int j = 1; j < mazeWay.GetLength(1) - 1; j++)
                {
                    if (mazeWay[i, j] == 0)
                    {
                        SetIndexArray(mazeWay, i, j, ++index);
                    }
                }
            }

            for (int i = 1; i < mazeWay.GetLength(0) - 1; i++)
            {
                for (int j = 1; j < mazeWay.GetLength(1) - 1; j++)
                {
                    if (mazeWay[i, j] == -1)
                    {
                        if (mazeWay[i + 1, j] != -1
                            && mazeWay[i - 1, j] != -1
                            && mazeWay[i + 1, j] != mazeWay[i - 1, j])
                        {
                            errorListPoints.Add(new Point(j, i));
                        }

                        if (mazeWay[i, j + 1] != -1
                         && mazeWay[i, j - 1] != -1
                         && mazeWay[i, j + 1] != mazeWay[i, j - 1])
                        {
                            errorListPoints.Add(new Point(j, i));
                        }
                    }
                }
            }

        }

        private void SetIndexArray(int[,] mazeWay, int i, int j, int index)
        {

            var listWay = new List<List<Point>>() {
                new List<Point>()
                {
                    new Point(i,j)
        }
    };

            while (listWay.Count != 0)
            {
                var currentWayList = listWay[listWay.Count - 1];
                var newWave = new List<Point>();

                currentWayList.ForEach((el) =>
                            {
                                mazeWay[el.X, el.Y] = index;

                                if (mazeWay[el.X + 1, el.Y] == 0)
                                {
                                    mazeWay[el.X + 1, el.Y] = index;
                                    newWave.Add(new Point(el.X + 1, el.Y));
                                }

                                if (mazeWay[el.X - 1, el.Y] == 0)
                                {
                                    mazeWay[el.X - 1, el.Y] = index;
                                    newWave.Add(new Point(el.X - 1, el.Y));
                                }

                                if (mazeWay[el.X, el.Y + 1] == 0)
                                {
                                    mazeWay[el.X, el.Y + 1] = index;
                                    newWave.Add(new Point(el.X, el.Y + 1));
                                }

                                if (mazeWay[el.X, el.Y - 1] == 0)
                                {
                                    mazeWay[el.X, el.Y - 1] = index;
                                    newWave.Add(new Point(el.X, el.Y - 1));
                                }
                            });

                listWay.RemoveAt(listWay.Count - 1);

                if (newWave.Count != 0)
                {
                    listWay.Add(newWave);
                }
            }
        }

        public void ClearWay()
        {
            SetPointEntry(Entry.X, Entry.Y);

            for (int i = 0; i < _mazeComponents.GetLength(0); i++)
            {
                for (int j = 0; j < _mazeComponents.GetLength(1); j++)
                {
                    if (_mazeComponents[i, j] == MazeComponent.Track 
                        || _mazeComponents[i, j] == MazeComponent.TrackDown
                        || _mazeComponents[i, j] == MazeComponent.TrackLeft
                        || _mazeComponents[i, j] == MazeComponent.TrackRight)
                        _mazeComponents[i, j] = MazeComponent.Pass;
                }
            }

            SetPointEntry(Entry.X, Entry.Y);
        }

        private bool CheckElementSet(int x, int y)
        {
            // проверяем квадраты на равенство, если в квадрате 4 одинаковых элемент значит тру
            return (_mazeComponents[y, x] == _mazeComponents[y + 1, x] //правый нижний квадрат
                    && _mazeComponents[y, x] == _mazeComponents[y, x + 1]
                    && _mazeComponents[y, x] == _mazeComponents[y + 1, x + 1])

                   || (_mazeComponents[y, x] == _mazeComponents[y - 1, x] // левый верхний квадрат
                       && _mazeComponents[y, x] == _mazeComponents[y, x - 1]
                       && _mazeComponents[y, x] == _mazeComponents[y - 1, x - 1])

                   || (_mazeComponents[y, x] == _mazeComponents[y - 1, x] // правый верхний квадрат
                       && _mazeComponents[y, x] == _mazeComponents[y, x + 1]
                       && _mazeComponents[y, x] == _mazeComponents[y - 1, x + 1])

                   || (_mazeComponents[y, x] == _mazeComponents[y + 1, x] // левый нижний квадрат
                       && _mazeComponents[y, x] == _mazeComponents[y, x - 1]
                       && _mazeComponents[y, x] == _mazeComponents[y + 1, x - 1]);
        }

        public void FindWayWave(Update updateMazeComponent, ref int speed) // поиск по волновому алгоритму
        {
            FindWayMaze.WaveWay(this, updateMazeComponent, ref speed);
        }

        public void FindWayOneHand(Update updateMazeComponent, ref int koefSpeed) // поиск по алгоритму одной руки
        {
            FindWayMaze.OneWayHand(this, updateMazeComponent, ref koefSpeed);
        }
    }
}
