﻿using MazeLibrary.Enums;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeLibrary.MazeClass
{
    public static class GenerateMaze
    {
        // Особенность алгоритма заключается в том, что у нас вход и выход должны стоять на нечётных позициях,
        // тогда гарантирована правильное построение лабиринта
        public static MazeComponent[,] GenerateMazePrim(MazeStruct mazeStruct, MazeStruct.Update updateMaze, ref int speed)
        {
            int width = mazeStruct.Width;
            int length = mazeStruct.Length;
            MazeComponent[,] maze = new MazeComponent[width, length];


            Random random = new Random();

            // устанавливаем стены по краю и выставляем свободные ячейки внутри бокса
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    if (i == 0 || j == 0 || i == width - 1 || j == length - 1)
                        maze[i, j] = MazeComponent.Wall;
                    else
                        maze[i, j] = MazeComponent.Track;
                }
            }

            maze[mazeStruct.GetEntry().Y, mazeStruct.GetEntry().X] = MazeComponent.Entry;
            maze[mazeStruct.GetExit().Y, mazeStruct.GetExit().X] = MazeComponent.Exit;
            maze[mazeStruct.GetPersonage().Y, mazeStruct.GetPersonage().X] = MazeComponent.Personage;

            var currentPosition = mazeStruct.GetPersonage();

            // Словарь точек и возможных путей из этих точек
            var dictonaryPointList = new Dictionary<Point, List<Point>>() { { currentPosition, new List<Point>() } };

            do
            {
                // Список возможных точек
                var keys = dictonaryPointList.Keys.ToList();

                // Выбираем случайным образом текущую точку
                currentPosition = keys[random.Next(0, keys.Count)];

                // устанавливаем проход в текущей точке
                maze[currentPosition.Y, currentPosition.X] = MazeComponent.Pass;

                // заводим словарь направлений
                var dictonaryWay = new Dictionary<string, Point>() {
                    {"Up" , new Point(currentPosition.X, currentPosition.Y - 2) },
                    {"Down", new Point(currentPosition.X, currentPosition.Y + 2) },
                    {"Left", new Point(currentPosition.X - 2, currentPosition.Y)},
                    {"Right", new Point(currentPosition.X + 2, currentPosition.Y)}
                };

                // Берём список путей текущей точки.
                var list = dictonaryPointList[currentPosition];

                // Добавляем в список пути, в которые мы можем перейти
                foreach (var el in dictonaryWay)
                    AddNewWay(width, length, maze, list, el.Value);

                // Список вершин в которые мы не можем перейти и мы их будем удалять
                var removeKey = new List<KeyValuePair<string, Point>>();

                // Проверяем вершины и если мы в них попасть не можем, то добавляем их в список для удаления
                foreach (var el in dictonaryWay)
                {
                    if (!list.Contains(el.Value))
                        removeKey.Add(el);
                }

                // Удаляем вершины
                removeKey.ForEach(el => dictonaryWay.Remove(el.Key));

                // Если список переходов равен 0, то мы удаляем текущую точку, так как у неё нет дальнейшего пути следования
                if (dictonaryPointList[currentPosition].Count == 0)
                    dictonaryPointList.Remove(currentPosition);

                // Если нет путей
                if (dictonaryWay.Count != 0)
                {
                    // заводит список направлений
                    var newWay = dictonaryWay.Keys.ToList()[random.Next(0, dictonaryWay.Count)];
                    // Выбираем путь в который мы пойдём
                    var currentWay = dictonaryWay[newWay];

                    // так как мы прошли в этот путь, то у всех остальных уже нет возможности попасть в это место
                    // Поэтому мы их удаляем
                    foreach (var el in dictonaryPointList)
                        el.Value.Remove(currentWay);

                    currentPosition = currentWay;

                    // если этой вершины нет в словаре, то добавляем её в словарь
                    if (!dictonaryPointList.ContainsKey(currentPosition))
                    {
                        dictonaryPointList.Add(currentPosition, new List<Point>());
                    }

                    // устанавливаем там проход
                    maze[currentWay.Y, currentWay.X] = MazeComponent.Pass;

                    // в зависимости от направления устанавливаем стены лабиринта
                    // и проход в промежутке
                    switch (newWay)
                    {
                        case "Up":
                            SetWall(currentWay.Y + 2, currentWay.X, maze);
                            SetWall(currentWay.Y + 1, currentWay.X, maze);
                            SetWall(currentWay.Y, currentWay.X, maze);

                            maze[currentWay.Y + 1, currentWay.X] = MazeComponent.Pass;


                            break;

                        case "Down":
                            SetWall(currentWay.Y - 2, currentWay.X, maze);
                            SetWall(currentWay.Y - 1, currentWay.X, maze);
                            SetWall(currentWay.Y, currentWay.X, maze);

                            maze[currentWay.Y - 1, currentWay.X] = MazeComponent.Pass;

                            break;

                        case "Left":
                            SetWall(currentWay.Y, currentWay.X + 2, maze);
                            SetWall(currentWay.Y, currentWay.X + 1, maze);
                            SetWall(currentWay.Y, currentWay.X, maze);

                            maze[currentWay.Y, currentWay.X + 1] = MazeComponent.Pass;

                            break;

                        case "Right":
                            SetWall(currentWay.Y, currentWay.X - 2, maze);
                            SetWall(currentWay.Y, currentWay.X - 1, maze);
                            SetWall(currentWay.Y, currentWay.X, maze);

                            maze[currentWay.Y, currentWay.X - 1] = MazeComponent.Pass;
                            break;
                    }
                }

                updateMaze(maze, ref speed);

            } while (dictonaryPointList.Count != 0);

            // Заполнение пустот в середине между 4 стенками
            for (int i = 1; i < width - 1; i++)
            {
                for (int j = 1; j < length - 1; j++)
                {
                    if (CheckCross(i, j, MazeComponent.Wall, maze))
                        maze[i, j] = MazeComponent.Wall;
                }
            }

            var listPoint = new List<Point>();

            // находим вершины в которые, по какой-то причине не попал алгоритм
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    if (maze[i, j] == MazeComponent.Track)
                    {
                        listPoint.Add(new Point(j, i));
                    }
                }
            }

            // Если можно поставить проход, то мы его ставим, иначе там ставится стена
            listPoint.ForEach(
                el =>
                {
                    if (CheckComponent(el.Y, el.X, MazeComponent.Pass, maze))
                        maze[el.Y, el.X] = MazeComponent.Pass;
                    else
                        maze[el.Y, el.X] = MazeComponent.Wall;
                }

            );


            maze[mazeStruct.GetPersonage().Y, mazeStruct.GetPersonage().X] = MazeComponent.Personage;
            updateMaze(maze, ref speed);

            return maze;
        }

        public static MazeComponent[,] GenerateMazeKruskala(MazeStruct mazeStruct, MazeStruct.Update updateMaze, ref int speed)
        {
            int width = mazeStruct.Width;
            int length = mazeStruct.Length;
            MazeComponent[,] maze = new MazeComponent[width, length];

            Random random = new Random();

            // устанавливаем стены по краю и выставляем свободные ячейки внутри бокса
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    if (i == 0 || j == 0 || i == width - 1 || j == length - 1)
                        maze[i, j] = MazeComponent.Wall;
                    else
                        maze[i, j] = MazeComponent.Track;
                }
            }

            maze[mazeStruct.GetEntry().Y, mazeStruct.GetEntry().X] = MazeComponent.Entry;
            maze[mazeStruct.GetExit().Y, mazeStruct.GetExit().X] = MazeComponent.Exit;

            // Лист стенок лабиринта
            List<Point> listWall = new List<Point>();

            for (int j = 2; j < length - 1; j += 2)
            {
                for (int i = 1; i < width - 1; i += 2)
                {
                    listWall.Add(new Point(j, i));

                }
            }

            //     Генерация стенок
            for (int i = 2; i < width - 1; i += 2)
            {
                for (int j = 1; j < length - 1; j += 2)
                {
                    listWall.Add(new Point(j, i));
                }
            }

            var listWayPointes = new List<List<Point>>();

            for (int i = 1; i < width - 1; i += 2)
            {
                for (int j = 1; j < length - 1; j += 2)
                {
                    listWayPointes.Add(new List<Point>()
                    {
                        new Point(j,i)
                    });
                }
            }

            do
            {
                var numberWall = random.Next(0, listWall.Count);
                // выбираем стенку.
                var pointWall = listWall[numberWall];
                listWall.RemoveAt(numberWall);

                // заводим словарь направлений
                var dictionaryWay = new Dictionary<string, List<Point>>() {
                    {"Vertical" , new List<Point>(){
                        new Point(pointWall.X, pointWall.Y - 1),
                        new Point(pointWall.X, pointWall.Y + 1)}
                    },
                    {"Horizotal" , new List<Point>(){
                        new Point(pointWall.X - 1, pointWall.Y),
                        new Point(pointWall.X + 1, pointWall.Y)
                    }
                }};

                // списоку возомжных путей
                var listPossibleWay = new List<List<Point>>();

                var removeDictonaryList = new List<KeyValuePair<string, List<Point>>>();

                foreach (var el in dictionaryWay)
                {
                    var list = el.Value;
                    if (FindListWayPointes(list[0], list[1], listWayPointes)
                        && (list[0].X != 0
                        && list[1].X < length - 1
                        && list[0].Y != 0
                        && list[1].Y < width - 1))
                    {
                        listPossibleWay.Add(el.Value);
                    }
                    else
                    {
                        removeDictonaryList.Add(el);
                    }
                }

                removeDictonaryList.ForEach(obj => dictionaryWay.Remove(obj.Key));

                if (listPossibleWay.Count != 0)
                {
                    var numberWay = random.Next(0, listPossibleWay.Count);
                    var directionWay = listPossibleWay[numberWay];

                    UdateListWayPoints(listWayPointes, directionWay[0], directionWay[1]);

                    if (directionWay[0].Y == directionWay[1].Y)
                    {
                        for (int x = directionWay[0].X; x <= directionWay[1].X; x++)
                        {
                            maze[directionWay[0].Y, x] = MazeComponent.Pass;
                            SetWall(directionWay[0].Y, x, maze);
                        }
                    }
                    else
                    {
                        for (int y = directionWay[0].Y; y <= directionWay[1].Y; y++)
                        {
                            maze[y, directionWay[0].X] = MazeComponent.Pass;
                            SetWall(y, directionWay[0].X, maze);
                        }
                    }



                }

                updateMaze(maze, ref speed);
            } while (listWall.Count != 0);

            for (int i = 2; i < width - 2; i += 2)
            {
                for (int j = 2; j < length - 2; j += 2)
                {
                    maze[i, j] = MazeComponent.Wall;
                }
            }

            maze[mazeStruct.GetPersonage().Y, mazeStruct.GetPersonage().X] = MazeComponent.Personage;
            updateMaze(maze, ref speed);

            return maze;
        }

        private static void UdateListWayPoints(List<List<Point>> listWayPointes, Point point1, Point point2)
        {
            int indexPoint1 = FindPointIndex(listWayPointes, point1);
            int indexPoint2 = FindPointIndex(listWayPointes, point2);

            if (indexPoint1 < listWayPointes.Count && indexPoint2 < listWayPointes.Count)
            {
                for (int i = 0; i < listWayPointes[indexPoint2].Count; i++)
                {
                    listWayPointes[indexPoint1].Add(listWayPointes[indexPoint2][i]);
                }

                listWayPointes.RemoveAt(indexPoint2);
            }

        }

        private static int FindPointIndex(List<List<Point>> listWayPointes, Point point1)
        {
            int indexPoint = 0;
            while (indexPoint < listWayPointes.Count)
            {
                if (listWayPointes[indexPoint].Where(obj => obj.X == point1.X && obj.Y == point1.Y).ToList().Count != 0)
                {
                    break;
                }
                else
                    indexPoint++;
            }

            return indexPoint;
        }

        private static bool FindListWayPointes(Point point1, Point point2, List<List<Point>> listWayPointes)
        {
            var listPoint1 = GetListPoint(point1, listWayPointes);
            var listPoint2 = GetListPoint(point2, listWayPointes);
            int indexPoint1 = FindPointIndex(listWayPointes, point1);
            int indexPoint2 = FindPointIndex(listWayPointes, point2);


            if (listPoint1.Count == 1 && listPoint2.Count == 1 && indexPoint1 != indexPoint2)
                return true;
            else
                return false;
        }

        private static List<List<Point>> GetListPoint(Point point1, List<List<Point>> listWayPointes)
        {
            return listWayPointes.Where(obj =>
            {
                return obj.Where(obj1 => obj1.X == point1.X && obj1.Y == point1.Y).ToList().Count != 0;
            }).ToList();
        }

        private static bool CheckComponentDiagonal(int x, int y, int widht, int length, MazeComponent component, MazeComponent[,] mazeComponents)
        {
            return (x - 1 >= 0 && y - 1 >= 0 && mazeComponents[y - 1, x - 1] == component)
                || (x - 1 >= 0 && y + 1 < widht && mazeComponents[y + 1, x - 1] == component)
                || (x + 1 < length && y + 1 < widht && mazeComponents[y + 1, x + 1] == component)
                || (x + 1 < length && y - 1 >= 0 && mazeComponents[y - 1, x + 1] == component);
        }


        private static void SetWall(int y, int x, MazeComponent[,] maze)
        {
            SetComponent(y + 1, x, maze);
            SetComponent(y - 1, x, maze);
            SetComponent(y, x + 1, maze);
            SetComponent(y, x - 1, maze);

        }

        private static void SetComponent(int y, int x, MazeComponent[,] maze)
        {
            if (maze[y, x] == MazeComponent.Track && CheckComponent(y, x, MazeComponent.Wall, maze))
                maze[y, x] = MazeComponent.Wall;
        }

        private static void AddNewWay(int width, int length, MazeComponent[,] maze, List<Point> list, Point newPoint)
        {
            if (newPoint.X > 0 && newPoint.X < length - 1 && newPoint.Y > 0 && newPoint.Y < width - 1
                                && (maze[newPoint.Y, newPoint.X] == MazeComponent.Track || maze[newPoint.Y, newPoint.X] == MazeComponent.Exit) && !CheckContainPoint(list, newPoint)
                                && CheckComponent(newPoint.Y, newPoint.X, MazeComponent.Pass, maze))
            {
                list.Add(newPoint);
            }
        }

        private static bool CheckContainPoint(List<Point> points, Point point)
        {
            foreach (var el in points)
                if (el.X == point.X && el.Y == point.Y)
                    return true;

            return false;
        }
        private static bool CheckComponent(int y, int x, MazeComponent component, MazeComponent[,] mazeComponents)
        {
            return (component != mazeComponents[y - 1, x - 1] || component != mazeComponents[y - 1, x] || component != mazeComponents[y, x - 1])
                && (component != mazeComponents[y - 1, x + 1] || component != mazeComponents[y - 1, x] || component != mazeComponents[y, x + 1])
                && (component != mazeComponents[y + 1, x + 1] || component != mazeComponents[y + 1, x] || component != mazeComponents[y, x + 1])
                && (component != mazeComponents[y + 1, x - 1] || component != mazeComponents[y + 1, x] || component != mazeComponents[y, x - 1]);
        }

        private static bool CheckCross(int y, int x, MazeComponent component, MazeComponent[,] maze)
        {
            return maze[y + 1, x] == component
                && maze[y - 1, x] == component
                 && maze[y, x + 1] == component
                 && maze[y, x - 1] == component;
        }
    }
}
