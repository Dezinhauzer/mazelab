﻿using MazeLibrary.MazeClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MazeLibrary.Database
{
    public class ModelMazeStruct
    {
        private CrudRepository _crudRepository;


        public ModelMazeStruct()
        {
            _crudRepository = new CrudRepository();
        }

        public List<MazeStruct> GetAll()
        {
            try
            {
                var allEntity = _crudRepository.GetAll();
                var listMazeStruct = new List<MazeStruct>();
                allEntity.ForEach(el =>
                {

                    listMazeStruct.Add(new MazeStruct(el));
                });

                return listMazeStruct;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка\n{ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw new Exception();

            }
        }



        public void Del(long currentIdMaze)
        {
            try
            {
                _crudRepository.DeleteAtElement(currentIdMaze);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка\n{ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AddOrEdit(MazeStruct maze)
        {
            try
            {
                var mazeEntity = maze.GetEntity();
                _crudRepository.AddElement(mazeEntity);
                maze.Id = mazeEntity.Id;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка\n{ex.Message}\nОшибка при добавление", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
