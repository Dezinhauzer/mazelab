﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;


namespace MazeLibrary.Database
{
    public class NHibernateHelper
    {
        public static ISession OpenSession()
        {
            try
            {

                string way = $"{Directory.GetCurrentDirectory()}\\Resource\\Database\\Maze_Resource_Database_MazeDB.mdf";
                string stringConnection = $"Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=\"{way}\";Integrated Security=True;Integrated Security=True;Connect Timeout=30";
                ISessionFactory sessionFactory = Fluently.Configure()
                    .Database(MsSqlConfiguration.MsSql2012
                    .ConnectionString(stringConnection).ShowSql()
                )
                .Mappings(m =>
                {
                    m.FluentMappings.AddFromAssemblyOf<MazeEntity>();
                })
                .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true))

                .BuildSessionFactory();

                return sessionFactory.OpenSession();
            }
            catch
            {
                throw new Exception("Ошибка при подключение к Базе Данных");
            }


        }


    }
}
