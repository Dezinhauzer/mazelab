﻿using FluentNHibernate.Mapping;
using MazeLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeLibrary.Database
{
    public class MazeEntity
    {
        public virtual long Id { get; set; }
        public virtual int Length { get; set; }
        public virtual int Width { get; set; }
        public virtual Themes Theme { get; set; }
        public virtual string ArrayMaze { get; set; }
    }

    public class MazeEntityMap : ClassMap<MazeEntity>
    {
        public MazeEntityMap()
        {
            Id(obj => obj.Id);
            Map(obj => obj.Length);
            Map(obj => obj.Width);
            Map(obj => obj.Theme);
            Map(obj => obj.ArrayMaze).CustomType("StringClob").CustomSqlType("nvarchar(max)");
        }
    }
}
