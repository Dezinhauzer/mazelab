﻿using NHibernate;
using NHibernate.Transform;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;


namespace MazeLibrary.Database
{
    public class CrudRepository
    {
        public CrudRepository() { }

        public void AddElement(MazeEntity hibernateNotes)
        {
            using (ISession s = NHibernateHelper.OpenSession())
            using (ITransaction tr = s.BeginTransaction())
            {
                s.SaveOrUpdate(hibernateNotes);
                tr.Commit();
            }
        }

        public List<MazeEntity> GetAll()
        {
            List<MazeEntity> notes;

            using (var session = NHibernateHelper.OpenSession())
            {
                notes = session.Query<MazeEntity>().ToList();
            }

            return notes;
        }

        public void EditAtElement(MazeEntity hibernateNotes)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Update(hibernateNotes);
                transaction.Commit();
            }
        }

        public void DeleteAtElement(long index)
        {
            using (ISession s = NHibernateHelper.OpenSession())
            using (ITransaction tr = s.BeginTransaction())
            {
                s.Delete(GetAtElement(index));
                tr.Commit();
            }
        }

        public MazeEntity GetAtElement(long index)
        {
            MazeEntity notes;
            using (ISession s = NHibernateHelper.OpenSession())
            {
                notes = s.Get<MazeEntity>(index);
            }

            return notes;
        }
    }

}
