﻿using System;

namespace MazeProject
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.NUDLength = new System.Windows.Forms.NumericUpDown();
            this.NUDWidth = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ButtonGenerate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.GBTheme = new System.Windows.Forms.GroupBox();
            this.ComboBoxTheme = new System.Windows.Forms.ComboBox();
            this.GBMaze = new System.Windows.Forms.GroupBox();
            this.GBPatternManualGen = new System.Windows.Forms.GroupBox();
            this.RBAlgPatternPass = new System.Windows.Forms.RadioButton();
            this.RBAlgPatternWall = new System.Windows.Forms.RadioButton();
            this.PictureBoxPass = new System.Windows.Forms.PictureBox();
            this.PictureBoxWall = new System.Windows.Forms.PictureBox();
            this.ButtonManualCheckRight = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.DBMenuToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStripLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStripSave = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStripSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.руководствоПользователяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StripMenuDevelopers = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GroupBoxArrange = new System.Windows.Forms.GroupBox();
            this.GBArrangePattern = new System.Windows.Forms.GroupBox();
            this.ButtonSetEntryExit = new System.Windows.Forms.Button();
            this.RBArrangePatternExit = new System.Windows.Forms.RadioButton();
            this.RBArrangePatternEntry = new System.Windows.Forms.RadioButton();
            this.PictureBoxEntry = new System.Windows.Forms.PictureBox();
            this.PictureBoxExit = new System.Windows.Forms.PictureBox();
            this.ButtonArrangeExitEntry = new System.Windows.Forms.Button();
            this.RBAutomaticGenerateEntryExit = new System.Windows.Forms.RadioButton();
            this.RBManualGenerateEntryExit = new System.Windows.Forms.RadioButton();
            this.GBAlgGen = new System.Windows.Forms.GroupBox();
            this.GBALgGenSpeed = new System.Windows.Forms.GroupBox();
            this.ButtonGenMaze = new System.Windows.Forms.Button();
            this.TrackBarAlgGenSpeed = new System.Windows.Forms.TrackBar();
            this.RBAlgGenKruskala = new System.Windows.Forms.RadioButton();
            this.RBAlgGenPrima = new System.Windows.Forms.RadioButton();
            this.RBAlgGenAutomatic = new System.Windows.Forms.RadioButton();
            this.RBAlgGenManual = new System.Windows.Forms.RadioButton();
            this.GBFindWay = new System.Windows.Forms.GroupBox();
            this.ButtonFindWay = new System.Windows.Forms.Button();
            this.GroupBoxSpeedFindWay = new System.Windows.Forms.GroupBox();
            this.TrackBarFindWaySpeed = new System.Windows.Forms.TrackBar();
            this.RBFindWayManual = new System.Windows.Forms.RadioButton();
            this.RBFindWayOneHand = new System.Windows.Forms.RadioButton();
            this.RBFindWayWave = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.NUDLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDWidth)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.GBTheme.SuspendLayout();
            this.GBPatternManualGen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxPass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxWall)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.GroupBoxArrange.SuspendLayout();
            this.GBArrangePattern.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxExit)).BeginInit();
            this.GBAlgGen.SuspendLayout();
            this.GBALgGenSpeed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarAlgGenSpeed)).BeginInit();
            this.GBFindWay.SuspendLayout();
            this.GroupBoxSpeedFindWay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarFindWaySpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // NUDLength
            // 
            this.NUDLength.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.NUDLength.Location = new System.Drawing.Point(6, 32);
            this.NUDLength.Maximum = new decimal(new int[] {
            45,
            0,
            0,
            0});
            this.NUDLength.Minimum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.NUDLength.Name = "NUDLength";
            this.NUDLength.ReadOnly = true;
            this.NUDLength.Size = new System.Drawing.Size(134, 20);
            this.NUDLength.TabIndex = 0;
            this.NUDLength.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // NUDWidth
            // 
            this.NUDWidth.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.NUDWidth.Location = new System.Drawing.Point(6, 74);
            this.NUDWidth.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.NUDWidth.Minimum = new decimal(new int[] {
            21,
            0,
            0,
            0});
            this.NUDWidth.Name = "NUDWidth";
            this.NUDWidth.ReadOnly = true;
            this.NUDWidth.Size = new System.Drawing.Size(134, 20);
            this.NUDWidth.TabIndex = 1;
            this.NUDWidth.Value = new decimal(new int[] {
            21,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Длина";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ширина";
            // 
            // ButtonGenerate
            // 
            this.ButtonGenerate.Location = new System.Drawing.Point(6, 156);
            this.ButtonGenerate.Name = "ButtonGenerate";
            this.ButtonGenerate.Size = new System.Drawing.Size(134, 28);
            this.ButtonGenerate.TabIndex = 4;
            this.ButtonGenerate.Text = "Генерация периметра";
            this.ButtonGenerate.UseVisualStyleBackColor = true;
            this.ButtonGenerate.Click += new System.EventHandler(this.ButtonGenerate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.GBTheme);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.ButtonGenerate);
            this.groupBox1.Controls.Add(this.NUDLength);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.NUDWidth);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(12, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(146, 196);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры";
            this.groupBox1.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // GBTheme
            // 
            this.GBTheme.Controls.Add(this.ComboBoxTheme);
            this.GBTheme.Location = new System.Drawing.Point(7, 101);
            this.GBTheme.Name = "GBTheme";
            this.GBTheme.Size = new System.Drawing.Size(134, 49);
            this.GBTheme.TabIndex = 9;
            this.GBTheme.TabStop = false;
            this.GBTheme.Text = "Тема";
            // 
            // ComboBoxTheme
            // 
            this.ComboBoxTheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxTheme.FormattingEnabled = true;
            this.ComboBoxTheme.Items.AddRange(new object[] {
            "Лето",
            "Зима",
            "Весна",
            "Осень"});
            this.ComboBoxTheme.Location = new System.Drawing.Point(6, 19);
            this.ComboBoxTheme.Name = "ComboBoxTheme";
            this.ComboBoxTheme.Size = new System.Drawing.Size(119, 21);
            this.ComboBoxTheme.TabIndex = 8;
            this.ComboBoxTheme.SelectedIndexChanged += new System.EventHandler(this.ComboBoxTheme_SelectedIndexChanged);
            // 
            // GBMaze
            // 
            this.GBMaze.Location = new System.Drawing.Point(164, 28);
            this.GBMaze.Name = "GBMaze";
            this.GBMaze.Size = new System.Drawing.Size(1046, 810);
            this.GBMaze.TabIndex = 0;
            this.GBMaze.TabStop = false;
            this.GBMaze.Text = "Лабиринт";
            // 
            // GBPatternManualGen
            // 
            this.GBPatternManualGen.Controls.Add(this.RBAlgPatternPass);
            this.GBPatternManualGen.Controls.Add(this.RBAlgPatternWall);
            this.GBPatternManualGen.Controls.Add(this.PictureBoxPass);
            this.GBPatternManualGen.Controls.Add(this.PictureBoxWall);
            this.GBPatternManualGen.Controls.Add(this.ButtonManualCheckRight);
            this.GBPatternManualGen.Location = new System.Drawing.Point(6, 65);
            this.GBPatternManualGen.Name = "GBPatternManualGen";
            this.GBPatternManualGen.Size = new System.Drawing.Size(132, 125);
            this.GBPatternManualGen.TabIndex = 0;
            this.GBPatternManualGen.TabStop = false;
            this.GBPatternManualGen.Text = "Шаблоны";
            this.GBPatternManualGen.Visible = false;
            // 
            // RBAlgPatternPass
            // 
            this.RBAlgPatternPass.AutoSize = true;
            this.RBAlgPatternPass.Location = new System.Drawing.Point(6, 42);
            this.RBAlgPatternPass.Name = "RBAlgPatternPass";
            this.RBAlgPatternPass.Size = new System.Drawing.Size(62, 17);
            this.RBAlgPatternPass.TabIndex = 10;
            this.RBAlgPatternPass.TabStop = true;
            this.RBAlgPatternPass.Text = "Проход";
            this.RBAlgPatternPass.UseVisualStyleBackColor = true;
            // 
            // RBAlgPatternWall
            // 
            this.RBAlgPatternWall.AutoSize = true;
            this.RBAlgPatternWall.Checked = true;
            this.RBAlgPatternWall.Location = new System.Drawing.Point(6, 19);
            this.RBAlgPatternWall.Name = "RBAlgPatternWall";
            this.RBAlgPatternWall.Size = new System.Drawing.Size(55, 17);
            this.RBAlgPatternWall.TabIndex = 9;
            this.RBAlgPatternWall.TabStop = true;
            this.RBAlgPatternWall.Text = "Стена";
            this.RBAlgPatternWall.UseVisualStyleBackColor = true;
            // 
            // PictureBoxPass
            // 
            this.PictureBoxPass.Location = new System.Drawing.Point(67, 43);
            this.PictureBoxPass.Name = "PictureBoxPass";
            this.PictureBoxPass.Size = new System.Drawing.Size(18, 17);
            this.PictureBoxPass.TabIndex = 6;
            this.PictureBoxPass.TabStop = false;
            // 
            // PictureBoxWall
            // 
            this.PictureBoxWall.Location = new System.Drawing.Point(61, 19);
            this.PictureBoxWall.Name = "PictureBoxWall";
            this.PictureBoxWall.Size = new System.Drawing.Size(18, 17);
            this.PictureBoxWall.TabIndex = 5;
            this.PictureBoxWall.TabStop = false;
            // 
            // ButtonManualCheckRight
            // 
            this.ButtonManualCheckRight.Location = new System.Drawing.Point(5, 66);
            this.ButtonManualCheckRight.Name = "ButtonManualCheckRight";
            this.ButtonManualCheckRight.Size = new System.Drawing.Size(115, 49);
            this.ButtonManualCheckRight.TabIndex = 4;
            this.ButtonManualCheckRight.Text = "Проверка структуры";
            this.ButtonManualCheckRight.UseVisualStyleBackColor = true;
            this.ButtonManualCheckRight.Click += new System.EventHandler(this.ButtonManualCheckRight_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DBMenuToolStrip,
            this.оПрограммеToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1226, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // DBMenuToolStrip
            // 
            this.DBMenuToolStrip.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStripLoad,
            this.MenuStripSave,
            this.MenuStripSaveAs});
            this.DBMenuToolStrip.Name = "DBMenuToolStrip";
            this.DBMenuToolStrip.Size = new System.Drawing.Size(86, 20);
            this.DBMenuToolStrip.Text = "База данных";
            // 
            // MenuStripLoad
            // 
            this.MenuStripLoad.Name = "MenuStripLoad";
            this.MenuStripLoad.Size = new System.Drawing.Size(153, 22);
            this.MenuStripLoad.Text = "Загрузить";
            this.MenuStripLoad.Click += new System.EventHandler(this.LoadMenuItem);
            // 
            // MenuStripSave
            // 
            this.MenuStripSave.Enabled = false;
            this.MenuStripSave.Name = "MenuStripSave";
            this.MenuStripSave.Size = new System.Drawing.Size(153, 22);
            this.MenuStripSave.Text = "Сохранить";
            this.MenuStripSave.Click += new System.EventHandler(this.MenuStripSave_Click);
            // 
            // MenuStripSaveAs
            // 
            this.MenuStripSaveAs.Enabled = false;
            this.MenuStripSaveAs.Name = "MenuStripSaveAs";
            this.MenuStripSaveAs.Size = new System.Drawing.Size(153, 22);
            this.MenuStripSaveAs.Text = "Сохранить как";
            this.MenuStripSaveAs.Click += new System.EventHandler(this.MenuStripSaveAs_Click);
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.руководствоПользователяToolStripMenuItem,
            this.StripMenuDevelopers});
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.оПрограммеToolStripMenuItem.Text = "Справка";
            // 
            // руководствоПользователяToolStripMenuItem
            // 
            this.руководствоПользователяToolStripMenuItem.Name = "руководствоПользователяToolStripMenuItem";
            this.руководствоПользователяToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.руководствоПользователяToolStripMenuItem.Text = "Руководство пользователя";
            this.руководствоПользователяToolStripMenuItem.Click += new System.EventHandler(this.РуководствоПользователяToolStripMenuItem_Click);
            // 
            // StripMenuDevelopers
            // 
            this.StripMenuDevelopers.Name = "StripMenuDevelopers";
            this.StripMenuDevelopers.Size = new System.Drawing.Size(221, 22);
            this.StripMenuDevelopers.Text = "Сведения о разработчиках";
            this.StripMenuDevelopers.Click += new System.EventHandler(this.StripMenuDevelopers_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.CloseMenuItem);
            // 
            // GroupBoxArrange
            // 
            this.GroupBoxArrange.Controls.Add(this.GBArrangePattern);
            this.GroupBoxArrange.Controls.Add(this.ButtonArrangeExitEntry);
            this.GroupBoxArrange.Controls.Add(this.RBAutomaticGenerateEntryExit);
            this.GroupBoxArrange.Controls.Add(this.RBManualGenerateEntryExit);
            this.GroupBoxArrange.Enabled = false;
            this.GroupBoxArrange.Location = new System.Drawing.Point(12, 228);
            this.GroupBoxArrange.Name = "GroupBoxArrange";
            this.GroupBoxArrange.Size = new System.Drawing.Size(146, 87);
            this.GroupBoxArrange.TabIndex = 7;
            this.GroupBoxArrange.TabStop = false;
            this.GroupBoxArrange.Text = "Способ расстановки";
            // 
            // GBArrangePattern
            // 
            this.GBArrangePattern.Controls.Add(this.ButtonSetEntryExit);
            this.GBArrangePattern.Controls.Add(this.RBArrangePatternExit);
            this.GBArrangePattern.Controls.Add(this.RBArrangePatternEntry);
            this.GBArrangePattern.Controls.Add(this.PictureBoxEntry);
            this.GBArrangePattern.Controls.Add(this.PictureBoxExit);
            this.GBArrangePattern.Location = new System.Drawing.Point(7, 89);
            this.GBArrangePattern.Name = "GBArrangePattern";
            this.GBArrangePattern.Size = new System.Drawing.Size(134, 96);
            this.GBArrangePattern.TabIndex = 0;
            this.GBArrangePattern.TabStop = false;
            this.GBArrangePattern.Text = "Шаблоны";
            // 
            // ButtonSetEntryExit
            // 
            this.ButtonSetEntryExit.Location = new System.Drawing.Point(6, 66);
            this.ButtonSetEntryExit.Name = "ButtonSetEntryExit";
            this.ButtonSetEntryExit.Size = new System.Drawing.Size(122, 23);
            this.ButtonSetEntryExit.TabIndex = 17;
            this.ButtonSetEntryExit.Text = "Установить";
            this.ButtonSetEntryExit.UseVisualStyleBackColor = true;
            this.ButtonSetEntryExit.Click += new System.EventHandler(this.ButtonSetEntryExit_Click);
            // 
            // RBArrangePatternExit
            // 
            this.RBArrangePatternExit.AutoSize = true;
            this.RBArrangePatternExit.Location = new System.Drawing.Point(7, 43);
            this.RBArrangePatternExit.Name = "RBArrangePatternExit";
            this.RBArrangePatternExit.Size = new System.Drawing.Size(57, 17);
            this.RBArrangePatternExit.TabIndex = 16;
            this.RBArrangePatternExit.TabStop = true;
            this.RBArrangePatternExit.Text = "Выход";
            this.RBArrangePatternExit.UseVisualStyleBackColor = true;
            // 
            // RBArrangePatternEntry
            // 
            this.RBArrangePatternEntry.AutoSize = true;
            this.RBArrangePatternEntry.Checked = true;
            this.RBArrangePatternEntry.Location = new System.Drawing.Point(7, 19);
            this.RBArrangePatternEntry.Name = "RBArrangePatternEntry";
            this.RBArrangePatternEntry.Size = new System.Drawing.Size(49, 17);
            this.RBArrangePatternEntry.TabIndex = 15;
            this.RBArrangePatternEntry.TabStop = true;
            this.RBArrangePatternEntry.Text = "Вход";
            this.RBArrangePatternEntry.UseVisualStyleBackColor = true;
            // 
            // PictureBoxEntry
            // 
            this.PictureBoxEntry.Location = new System.Drawing.Point(57, 20);
            this.PictureBoxEntry.Name = "PictureBoxEntry";
            this.PictureBoxEntry.Size = new System.Drawing.Size(18, 17);
            this.PictureBoxEntry.TabIndex = 13;
            this.PictureBoxEntry.TabStop = false;
            // 
            // PictureBoxExit
            // 
            this.PictureBoxExit.Location = new System.Drawing.Point(62, 43);
            this.PictureBoxExit.Name = "PictureBoxExit";
            this.PictureBoxExit.Size = new System.Drawing.Size(18, 17);
            this.PictureBoxExit.TabIndex = 14;
            this.PictureBoxExit.TabStop = false;
            // 
            // ButtonArrangeExitEntry
            // 
            this.ButtonArrangeExitEntry.Location = new System.Drawing.Point(6, 61);
            this.ButtonArrangeExitEntry.Name = "ButtonArrangeExitEntry";
            this.ButtonArrangeExitEntry.Size = new System.Drawing.Size(134, 22);
            this.ButtonArrangeExitEntry.TabIndex = 5;
            this.ButtonArrangeExitEntry.Text = "Расставить";
            this.ButtonArrangeExitEntry.UseVisualStyleBackColor = true;
            this.ButtonArrangeExitEntry.Click += new System.EventHandler(this.ButtonArrangeExitEntry_Click_1);
            // 
            // RBAutomaticGenerateEntryExit
            // 
            this.RBAutomaticGenerateEntryExit.AutoSize = true;
            this.RBAutomaticGenerateEntryExit.Checked = true;
            this.RBAutomaticGenerateEntryExit.Location = new System.Drawing.Point(6, 19);
            this.RBAutomaticGenerateEntryExit.Name = "RBAutomaticGenerateEntryExit";
            this.RBAutomaticGenerateEntryExit.Size = new System.Drawing.Size(103, 17);
            this.RBAutomaticGenerateEntryExit.TabIndex = 1;
            this.RBAutomaticGenerateEntryExit.TabStop = true;
            this.RBAutomaticGenerateEntryExit.Text = "Автоматичесий";
            this.RBAutomaticGenerateEntryExit.UseVisualStyleBackColor = true;
            this.RBAutomaticGenerateEntryExit.CheckedChanged += new System.EventHandler(this.RBManualGenerateEntryExit_CheckedChanged);
            this.RBAutomaticGenerateEntryExit.Click += new System.EventHandler(this.RBAutomaticGenerateEntryExit_Click);
            // 
            // RBManualGenerateEntryExit
            // 
            this.RBManualGenerateEntryExit.AutoSize = true;
            this.RBManualGenerateEntryExit.Location = new System.Drawing.Point(6, 42);
            this.RBManualGenerateEntryExit.Name = "RBManualGenerateEntryExit";
            this.RBManualGenerateEntryExit.Size = new System.Drawing.Size(60, 17);
            this.RBManualGenerateEntryExit.TabIndex = 0;
            this.RBManualGenerateEntryExit.Text = "Ручной";
            this.RBManualGenerateEntryExit.UseVisualStyleBackColor = true;
            this.RBManualGenerateEntryExit.CheckedChanged += new System.EventHandler(this.RBManualGenerateEntryExit_CheckedChanged);
            this.RBManualGenerateEntryExit.Click += new System.EventHandler(this.RBAutomaticGenerateEntryExit_Click);
            // 
            // GBAlgGen
            // 
            this.GBAlgGen.Controls.Add(this.GBALgGenSpeed);
            this.GBAlgGen.Controls.Add(this.RBAlgGenAutomatic);
            this.GBAlgGen.Controls.Add(this.GBPatternManualGen);
            this.GBAlgGen.Controls.Add(this.RBAlgGenManual);
            this.GBAlgGen.Enabled = false;
            this.GBAlgGen.Location = new System.Drawing.Point(12, 321);
            this.GBAlgGen.Name = "GBAlgGen";
            this.GBAlgGen.Size = new System.Drawing.Size(145, 219);
            this.GBAlgGen.TabIndex = 8;
            this.GBAlgGen.TabStop = false;
            this.GBAlgGen.Text = "Алгоритмы генерации";
            // 
            // GBALgGenSpeed
            // 
            this.GBALgGenSpeed.Controls.Add(this.ButtonGenMaze);
            this.GBALgGenSpeed.Controls.Add(this.TrackBarAlgGenSpeed);
            this.GBALgGenSpeed.Controls.Add(this.RBAlgGenKruskala);
            this.GBALgGenSpeed.Controls.Add(this.RBAlgGenPrima);
            this.GBALgGenSpeed.Location = new System.Drawing.Point(6, 62);
            this.GBALgGenSpeed.Name = "GBALgGenSpeed";
            this.GBALgGenSpeed.Size = new System.Drawing.Size(133, 146);
            this.GBALgGenSpeed.TabIndex = 4;
            this.GBALgGenSpeed.TabStop = false;
            this.GBALgGenSpeed.Text = "Выбор алгоритма";
            // 
            // ButtonGenMaze
            // 
            this.ButtonGenMaze.Location = new System.Drawing.Point(27, 117);
            this.ButtonGenMaze.Name = "ButtonGenMaze";
            this.ButtonGenMaze.Size = new System.Drawing.Size(75, 23);
            this.ButtonGenMaze.TabIndex = 4;
            this.ButtonGenMaze.Text = "Генерация";
            this.ButtonGenMaze.UseVisualStyleBackColor = true;
            this.ButtonGenMaze.Click += new System.EventHandler(this.ButtonGenMaze_Click);
            // 
            // TrackBarAlgGenSpeed
            // 
            this.TrackBarAlgGenSpeed.Location = new System.Drawing.Point(7, 66);
            this.TrackBarAlgGenSpeed.Maximum = 2;
            this.TrackBarAlgGenSpeed.Name = "TrackBarAlgGenSpeed";
            this.TrackBarAlgGenSpeed.Size = new System.Drawing.Size(120, 45);
            this.TrackBarAlgGenSpeed.SmallChange = 50;
            this.TrackBarAlgGenSpeed.TabIndex = 3;
            this.TrackBarAlgGenSpeed.ValueChanged += new System.EventHandler(this.TrackBarAlgGenSpeed_ValueChanged);
            // 
            // RBAlgGenKruskala
            // 
            this.RBAlgGenKruskala.AutoSize = true;
            this.RBAlgGenKruskala.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F);
            this.RBAlgGenKruskala.Location = new System.Drawing.Point(3, 37);
            this.RBAlgGenKruskala.Name = "RBAlgGenKruskala";
            this.RBAlgGenKruskala.Size = new System.Drawing.Size(122, 17);
            this.RBAlgGenKruskala.TabIndex = 1;
            this.RBAlgGenKruskala.Text = "Алгоритм Краскала";
            this.RBAlgGenKruskala.UseVisualStyleBackColor = true;
            // 
            // RBAlgGenPrima
            // 
            this.RBAlgGenPrima.AutoSize = true;
            this.RBAlgGenPrima.Checked = true;
            this.RBAlgGenPrima.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F);
            this.RBAlgGenPrima.Location = new System.Drawing.Point(3, 14);
            this.RBAlgGenPrima.Name = "RBAlgGenPrima";
            this.RBAlgGenPrima.Size = new System.Drawing.Size(107, 17);
            this.RBAlgGenPrima.TabIndex = 0;
            this.RBAlgGenPrima.TabStop = true;
            this.RBAlgGenPrima.Text = "Алгоритм Прима";
            this.RBAlgGenPrima.UseVisualStyleBackColor = true;
            // 
            // RBAlgGenAutomatic
            // 
            this.RBAlgGenAutomatic.AutoSize = true;
            this.RBAlgGenAutomatic.Checked = true;
            this.RBAlgGenAutomatic.Location = new System.Drawing.Point(7, 19);
            this.RBAlgGenAutomatic.Name = "RBAlgGenAutomatic";
            this.RBAlgGenAutomatic.Size = new System.Drawing.Size(109, 17);
            this.RBAlgGenAutomatic.TabIndex = 3;
            this.RBAlgGenAutomatic.TabStop = true;
            this.RBAlgGenAutomatic.Text = "Автоматический";
            this.RBAlgGenAutomatic.UseVisualStyleBackColor = true;
            this.RBAlgGenAutomatic.CheckedChanged += new System.EventHandler(this.RBAlgGenAutomatic_CheckedChanged);
            // 
            // RBAlgGenManual
            // 
            this.RBAlgGenManual.AutoSize = true;
            this.RBAlgGenManual.Location = new System.Drawing.Point(7, 42);
            this.RBAlgGenManual.Name = "RBAlgGenManual";
            this.RBAlgGenManual.Size = new System.Drawing.Size(60, 17);
            this.RBAlgGenManual.TabIndex = 2;
            this.RBAlgGenManual.Text = "Ручной";
            this.RBAlgGenManual.UseVisualStyleBackColor = true;
            this.RBAlgGenManual.CheckedChanged += new System.EventHandler(this.RBAlgGenAutomatic_CheckedChanged);
            // 
            // GBFindWay
            // 
            this.GBFindWay.Controls.Add(this.ButtonFindWay);
            this.GBFindWay.Controls.Add(this.GroupBoxSpeedFindWay);
            this.GBFindWay.Controls.Add(this.RBFindWayManual);
            this.GBFindWay.Controls.Add(this.RBFindWayOneHand);
            this.GBFindWay.Controls.Add(this.RBFindWayWave);
            this.GBFindWay.Enabled = false;
            this.GBFindWay.Location = new System.Drawing.Point(12, 546);
            this.GBFindWay.Name = "GBFindWay";
            this.GBFindWay.Size = new System.Drawing.Size(145, 113);
            this.GBFindWay.TabIndex = 9;
            this.GBFindWay.TabStop = false;
            this.GBFindWay.Text = "Поиск пути";
            // 
            // ButtonFindWay
            // 
            this.ButtonFindWay.Location = new System.Drawing.Point(32, 84);
            this.ButtonFindWay.Name = "ButtonFindWay";
            this.ButtonFindWay.Size = new System.Drawing.Size(75, 23);
            this.ButtonFindWay.TabIndex = 6;
            this.ButtonFindWay.Text = "Поиск пути";
            this.ButtonFindWay.UseVisualStyleBackColor = true;
            this.ButtonFindWay.Click += new System.EventHandler(this.ButtonFindWay_Click);
            // 
            // GroupBoxSpeedFindWay
            // 
            this.GroupBoxSpeedFindWay.Controls.Add(this.TrackBarFindWaySpeed);
            this.GroupBoxSpeedFindWay.Location = new System.Drawing.Point(6, 103);
            this.GroupBoxSpeedFindWay.Name = "GroupBoxSpeedFindWay";
            this.GroupBoxSpeedFindWay.Size = new System.Drawing.Size(128, 68);
            this.GroupBoxSpeedFindWay.TabIndex = 5;
            this.GroupBoxSpeedFindWay.TabStop = false;
            this.GroupBoxSpeedFindWay.Text = "Скорость поиска";
            this.GroupBoxSpeedFindWay.Visible = false;
            // 
            // TrackBarFindWaySpeed
            // 
            this.TrackBarFindWaySpeed.Location = new System.Drawing.Point(7, 19);
            this.TrackBarFindWaySpeed.Maximum = 2;
            this.TrackBarFindWaySpeed.Name = "TrackBarFindWaySpeed";
            this.TrackBarFindWaySpeed.Size = new System.Drawing.Size(115, 45);
            this.TrackBarFindWaySpeed.SmallChange = 50;
            this.TrackBarFindWaySpeed.TabIndex = 3;
            this.TrackBarFindWaySpeed.ValueChanged += new System.EventHandler(this.TrackBarFindWay_ValueChanged);
            // 
            // RBFindWayManual
            // 
            this.RBFindWayManual.AutoSize = true;
            this.RBFindWayManual.Checked = true;
            this.RBFindWayManual.Location = new System.Drawing.Point(6, 17);
            this.RBFindWayManual.Name = "RBFindWayManual";
            this.RBFindWayManual.Size = new System.Drawing.Size(60, 17);
            this.RBFindWayManual.TabIndex = 2;
            this.RBFindWayManual.TabStop = true;
            this.RBFindWayManual.Text = "Ручной";
            this.RBFindWayManual.UseVisualStyleBackColor = true;
            this.RBFindWayManual.CheckedChanged += new System.EventHandler(this.RBFindWayManual_CheckedChanged);
            // 
            // RBFindWayOneHand
            // 
            this.RBFindWayOneHand.AutoSize = true;
            this.RBFindWayOneHand.Location = new System.Drawing.Point(6, 63);
            this.RBFindWayOneHand.Name = "RBFindWayOneHand";
            this.RBFindWayOneHand.Size = new System.Drawing.Size(83, 17);
            this.RBFindWayOneHand.TabIndex = 1;
            this.RBFindWayOneHand.Text = "Одной руки";
            this.RBFindWayOneHand.UseVisualStyleBackColor = true;
            this.RBFindWayOneHand.CheckedChanged += new System.EventHandler(this.RBFindWayManual_CheckedChanged);
            // 
            // RBFindWayWave
            // 
            this.RBFindWayWave.AutoSize = true;
            this.RBFindWayWave.Location = new System.Drawing.Point(6, 40);
            this.RBFindWayWave.Name = "RBFindWayWave";
            this.RBFindWayWave.Size = new System.Drawing.Size(74, 17);
            this.RBFindWayWave.TabIndex = 0;
            this.RBFindWayWave.Text = "Волновой";
            this.RBFindWayWave.UseVisualStyleBackColor = true;
            this.RBFindWayWave.CheckedChanged += new System.EventHandler(this.RBFindWayManual_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1226, 848);
            this.Controls.Add(this.GBAlgGen);
            this.Controls.Add(this.GBFindWay);
            this.Controls.Add(this.GroupBoxArrange);
            this.Controls.Add(this.GBMaze);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1242, 887);
            this.MinimumSize = new System.Drawing.Size(1242, 887);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабиринт";
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpEvent);
            ((System.ComponentModel.ISupportInitialize)(this.NUDLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDWidth)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.GBTheme.ResumeLayout(false);
            this.GBPatternManualGen.ResumeLayout(false);
            this.GBPatternManualGen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxPass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxWall)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.GroupBoxArrange.ResumeLayout(false);
            this.GroupBoxArrange.PerformLayout();
            this.GBArrangePattern.ResumeLayout(false);
            this.GBArrangePattern.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxExit)).EndInit();
            this.GBAlgGen.ResumeLayout(false);
            this.GBAlgGen.PerformLayout();
            this.GBALgGenSpeed.ResumeLayout(false);
            this.GBALgGenSpeed.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarAlgGenSpeed)).EndInit();
            this.GBFindWay.ResumeLayout(false);
            this.GBFindWay.PerformLayout();
            this.GroupBoxSpeedFindWay.ResumeLayout(false);
            this.GroupBoxSpeedFindWay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarFindWaySpeed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown NUDLength;
        private System.Windows.Forms.NumericUpDown NUDWidth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ButtonGenerate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox GBMaze;
        private System.Windows.Forms.GroupBox GBPatternManualGen;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem DBMenuToolStrip;
        private System.Windows.Forms.ToolStripMenuItem MenuStripLoad;
        private System.Windows.Forms.ToolStripMenuItem MenuStripSave;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem руководствоПользователяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StripMenuDevelopers;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.GroupBox GroupBoxArrange;
        private System.Windows.Forms.ComboBox ComboBoxTheme;
        private System.Windows.Forms.RadioButton RBAutomaticGenerateEntryExit;
        private System.Windows.Forms.RadioButton RBManualGenerateEntryExit;
        private System.Windows.Forms.Button ButtonArrangeExitEntry;
        private System.Windows.Forms.GroupBox GBTheme;
        private System.Windows.Forms.GroupBox GBAlgGen;
        private System.Windows.Forms.RadioButton RBAlgGenManual;
        private System.Windows.Forms.RadioButton RBAlgGenKruskala;
        private System.Windows.Forms.RadioButton RBAlgGenPrima;
        private System.Windows.Forms.TrackBar TrackBarAlgGenSpeed;
        private System.Windows.Forms.GroupBox GBALgGenSpeed;
        private System.Windows.Forms.Button ButtonManualCheckRight;
        private System.Windows.Forms.GroupBox GBArrangePattern;
        private System.Windows.Forms.GroupBox GBFindWay;
        private System.Windows.Forms.GroupBox GroupBoxSpeedFindWay;
        private System.Windows.Forms.TrackBar TrackBarFindWaySpeed;
        private System.Windows.Forms.RadioButton RBFindWayManual;
        private System.Windows.Forms.RadioButton RBFindWayOneHand;
        private System.Windows.Forms.RadioButton RBFindWayWave;
        private System.Windows.Forms.Button ButtonGenMaze;
        private System.Windows.Forms.RadioButton RBAlgGenAutomatic;
        private System.Windows.Forms.Button ButtonFindWay;
        private System.Windows.Forms.RadioButton RBAlgPatternPass;
        private System.Windows.Forms.RadioButton RBAlgPatternWall;
        private System.Windows.Forms.PictureBox PictureBoxPass;
        private System.Windows.Forms.PictureBox PictureBoxWall;
        private System.Windows.Forms.RadioButton RBArrangePatternExit;
        private System.Windows.Forms.RadioButton RBArrangePatternEntry;
        private System.Windows.Forms.PictureBox PictureBoxEntry;
        private System.Windows.Forms.PictureBox PictureBoxExit;
        private System.Windows.Forms.Button ButtonSetEntryExit;
        private System.Windows.Forms.ToolStripMenuItem MenuStripSaveAs;
    }
}

