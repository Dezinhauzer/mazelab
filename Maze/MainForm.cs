﻿using MazeLibrary.Enums;
using MazeLibrary.Database;
using MazeLibrary.MazeClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using Maze;

namespace MazeProject
{
    public partial class MainForm : Form
    {
        private MazeStruct _maze;
        private PictureBox[,] _mazePictureBox;
        delegate void SetPosition(int x, int y);
        private readonly Dictionary<string, Themes> _dictionaryTheme;
        private Dictionary<RadioButton, SetPosition> _radioButtonMethod;
        private Dictionary<Themes, string> _dictionaryLocalizationTheme;
        private readonly ModelMazeStruct _modelMazeStruct;
        private readonly List<RadioButton> _listPatternAlgRadioButtons;
        private Dictionary<Themes, Dictionary<MazeComponent, Image>> _imageMazeComponent;
        private bool _isManualFindWay = false;

        public MainForm()
        {
            InitializeComponent();


            _maze = new MazeStruct();
            _modelMazeStruct = new ModelMazeStruct();

            this.DoubleBuffered = true;

            ComboBoxTheme.Text = GetSeason();

            _listPatternAlgRadioButtons = new List<RadioButton>()
            {
                RBAlgPatternWall,
                RBAlgPatternPass
            };

            _dictionaryTheme = new Dictionary<string, Themes>()
            {
                {"Осень",Themes.Autumn },
                {"Весна",Themes.Spring },
                {"Лето",Themes.Summer },
                {"Зима",Themes.Winter }
            };

            _dictionaryLocalizationTheme = new Dictionary<Themes, string>()
             {
                {Themes.Autumn, "Осень"},
                {Themes.Spring, "Весна"},
                {Themes.Summer, "Лето"},
                {Themes.Winter, "Зима"}
            };

            var currentDirectory = Directory.GetCurrentDirectory();


            _imageMazeComponent = new Dictionary<Themes, Dictionary<MazeComponent, Image>>();

            _dictionaryLocalizationTheme.Keys.ToList().ForEach(elThemes =>
            {
                _imageMazeComponent.Add(elThemes, new Dictionary<MazeComponent, Image>());
                Enum.GetNames(typeof(MazeComponent)).ToList().ForEach(elmazeComponent =>
                {

                    var component = (MazeComponent)(Enum.Parse(typeof(MazeComponent), elmazeComponent));
                    _imageMazeComponent[elThemes]
                    .Add(
                        component,
                        new Bitmap(new Bitmap(GetWayByThemeComponent(currentDirectory, elThemes, component)), new Size(20, 20))
                        );
                });
            });

            RadioButtonDelegateUpdate();
        }

        private string GetWayByThemeComponent(string currentDirectory, Themes elThemes, MazeComponent elmazeComponent)
        {
            return $"{currentDirectory}\\Resource\\Images\\Themes\\{elThemes.ToString()}\\{elmazeComponent.ToString()}.jpg";
        }

        private string GetSeason()
        {
            int month = DateTime.Now.Month;

            if (month >= 6 && month <= 8)
                return "Лето";
            else if (month >= 3 && month <= 5)
                return "Весна";
            else if (month >= 9 && month <= 11)
                return "Осень";
            else
                return "Зима";
        }

        private void RadioButtonDelegateUpdate()
        {
            _radioButtonMethod = new Dictionary<RadioButton, SetPosition>
            {
                { RBArrangePatternEntry, _maze.SetPointEntry},
                { RBArrangePatternExit, _maze.SetPointExit},
                { RBAlgPatternWall,_maze.SetPointWall},
                { RBAlgPatternPass,_maze.SetPointPass}
        };
        }

        private void ButtonGenerate_Click(object sender, EventArgs e)
        {

            _theme = ComboBoxTheme.Text;
            _speedAnimationFindWay = 200 - 65 * (TrackBarFindWaySpeed.Value + 1);
            GroupBoxArrange.Enabled = true;

            MenuStripSave.Enabled =
                MenuStripSaveAs.Enabled =
                GBFindWay.Enabled =
                GBAlgGen.Enabled =
                MenuStripSave.Enabled = false;

            UpdatePictureBoxGroupBox();
            GenerateMazeBox();
        }

        private void GenerateMazeBox()
        {
            var length = (int)NUDLength.Value;
            var width = (int)NUDWidth.Value;
            _maze = new MazeStruct(length, width, _dictionaryTheme[ComboBoxTheme.Text]);
            _maze.SetQuestion();

            InitialPictureBox(length, width);

            RadioButtonDelegateUpdate();
            int sp = 40;
            UpdateMazeComponent(_maze.GetMaze(), ref sp);
        }

        private void InitialPictureBox(int length, int width)
        {
            MazePictureBoxClear();
            _mazePictureBox = new PictureBox[width, length];
            int j1;
            var i1 = 0;

            for (int i = 0; i < width; i++)
            {
                j1 = 0;
                for (int j = 0; j < length; j++)
                {
                    _mazePictureBox[i, j] = CreatePictureBox(
                        (GBMaze.Width - length - length * 20) / 2 + j * 20 + j1,
                        (GBMaze.Height - width - width * 20) / 2 + i * 20 + i1,
                        i, j);
                    j1++;
                }
                i1++;
            }
        }

        private string _theme;

        public void UpdateMazeComponent(MazeComponent[,] maze, ref int speed)
        {
            for (var i = 0; i < _mazePictureBox.GetLength(0); i++)
            {
                for (var j = 0; j < _mazePictureBox.GetLength(1); j++)
                {
                    var image = _imageMazeComponent[_dictionaryTheme[_theme]][maze[i, j]];

                    if (_mazePictureBox[i, j].Image != image)
                    {

                        _mazePictureBox[i, j].Invoke(new MethodInvoker(() => _mazePictureBox[i, j].Image = image));

                    }
                }
            }
            Thread.Sleep(speed);
            GBMaze.Invoke(new MethodInvoker(() => GBMaze.Update()));
        }

        private void MazePictureBoxClear()
        {
            if (_mazePictureBox != null)
            {
                foreach (var el in _mazePictureBox)
                {
                    GBMaze.Controls.Remove(el);
                }
            }
        }

        private PictureBox CreatePictureBox(int x, int y, int i, int j)
        {
            PictureBox newCell = new PictureBox()
            {
                Location = new Point(x, y),
                Name = $"{i},{j}",
                Size = new Size(20, 20),
                Image = _imageMazeComponent[_dictionaryTheme[ComboBoxTheme.Text]][_maze.GetMaze()[i, j]]
            };
            newCell.Click += MouseClickGenerate;
            GBMaze.Controls.Add(newCell);
            return newCell;
        }

        private void MouseClickGenerate(object sender, EventArgs e)
        {
            var pb = (PictureBox)sender;
            var el = pb.Name.Split(',').Select(int.Parse).ToArray();
            pb.BorderStyle = BorderStyle.None;

            if (RBManualGenerateEntryExit.Checked && GroupBoxArrange.Enabled)
            {
                _radioButtonMethod[
                    RBArrangePatternEntry.Checked ? RBArrangePatternEntry : RBArrangePatternExit
                ](el[1], el[0]);

                _maze.SetQuestion();

            }

            if (GBAlgGen.Enabled && RBAlgGenManual.Checked)
            {
                _radioButtonMethod[_listPatternAlgRadioButtons.Where(obj => obj.Checked).ToList()[0]](el[1], el[0]);
                GBFindWay.Enabled = false;
            }

            UpdateMaze();
        }

        private void UpdateMaze()
        {
            for (int i = 0; i < _mazePictureBox.GetLength(0); i++)
            {
                for (int j = 0; j < _mazePictureBox.GetLength(1); j++)
                {
                    var image = _imageMazeComponent[_dictionaryTheme[ComboBoxTheme.Text]][_maze.GetMaze()[i, j]]; ;
                    if (_mazePictureBox[i, j].Image != image)
                    {
                        _mazePictureBox[i, j].Image = image;
                    }

                }
            }

        }

        private void ButtonStartGame_Click(object sender, EventArgs e)
        {
            NUDLength.Enabled = NUDWidth.Enabled = false;
        }

        private void KeyUpEvent(object sender, KeyEventArgs e)
        {
            if (RBFindWayManual.Checked && GBFindWay.Enabled && _isManualFindWay)
            {
                var personage = _maze.GetPersonage();
                Point newPoint = new Point();
                switch (e.KeyData)
                {
                    case Keys.W:
                        newPoint = new Point(personage.X, personage.Y - 1);

                        break;

                    case Keys.S:
                        newPoint = new Point(personage.X, personage.Y + 1);

                        break;

                    case Keys.A:
                        newPoint = new Point(personage.X - 1, personage.Y);

                        break;

                    case Keys.D:
                        newPoint = new Point(personage.X + 1, personage.Y);

                        break;
                }

                if (_maze.SetMovePersonage(newPoint.X, newPoint.Y))
                {
                    _isManualFindWay = false;
                    MessageBox.Show("Выход успешно найден");
                }

                UpdateMaze();
            }
        }

        private void CloseMenuItem(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonArrangeExitEntry_Click(object sender, EventArgs e)
        {
            if (RBAutomaticGenerateEntryExit.Checked)
            {
                _maze.AutoSetEntryExit();
                GBAlgGen.Enabled = true;
                int sp = 40;
                UpdateMazeComponent(_maze.GetMaze(), ref sp);
            }
            else
            {

            }

        }

        private void ButtonManualCheckRight_Click(object sender, EventArgs e)
        {
            _maze.ClearWay();
            UpdateMaze();

            foreach (var pictureBox in _mazePictureBox)
            {
                if (pictureBox.BorderStyle != BorderStyle.None)
                {
                    pictureBox.BorderStyle = BorderStyle.None;
                }
            }
            var listError = _maze.ExcelentStruct();

            if (listError.Count == 0)
            {
                GBFindWay.Enabled = true;

            }
            else
            {
                foreach (var point in listError)
                {
                    _mazePictureBox[point.Y, point.X].BorderStyle = BorderStyle.Fixed3D;
                }

                MessageBox.Show("При генерации лабиринта циклов существовать не должно", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }

        private int _speedAnimationAlgGen;

        private void ButtonGenMaze_Click(object sender, EventArgs e)
        {
            int koefSpeed = 10;

            _maze.ClearWay();

            if (RBAlgGenAutomatic.Checked)
            {
                foreach (var pictureBox in _mazePictureBox)
                {
                    pictureBox.BorderStyle = BorderStyle.None;
                }

                _speedAnimationAlgGen = 30 - koefSpeed * (TrackBarAlgGenSpeed.Value + 1);
                ThreadPool.QueueUserWorkItem(new WaitCallback(delegate (object unused)
                {
                    this.Invoke(new Action(delegate
                    {
                        (new System.Threading.Thread(delegate ()
                        {

                            if (RBAlgGenPrima.Checked)
                            {
                                SetButtonEnabled(false);
                                _maze.GeneratePrim(UpdateMazeComponent, ref _speedAnimationAlgGen);
                                SetButtonEnabled(true);
                            }
                            else if (RBAlgGenKruskala.Checked)
                            {
                                SetButtonEnabled(false);
                                _maze.GenerateKruskala(UpdateMazeComponent, ref _speedAnimationAlgGen);
                                SetButtonEnabled(true);
                            }

                            GBFindWay.Invoke(new MethodInvoker(() => GBFindWay.Enabled = true));
                        })).Start();
                    }));
                }));


            }
        }

        private void LoadMenuItem(object sender, EventArgs e)
        {
            try
            {
                DataBaseForm dataBaseForm = new DataBaseForm(_modelMazeStruct, _imageMazeComponent);
                Hide();

                MenuStripSave.Enabled = true;

                if (dataBaseForm.ShowDialog() == DialogResult.OK)
                {
                    _maze = dataBaseForm.ChoiceMaze;

                    NUDLength.Value = _maze.Length;
                    NUDWidth.Value = _maze.Width;
                    ComboBoxTheme.Text = _dictionaryLocalizationTheme[_maze.Theme];
                    InitialPictureBox(_maze.Length, _maze.Width);

                    _maze.ClearWay();
                    UpdateMaze();
                    int sp = 40;
                    UpdateMazeComponent(_maze.GetMaze(), ref sp);
                    GBAlgGen.Enabled = GBFindWay.Enabled = true;
                    RadioButtonDelegateUpdate();

                    MenuStripSave.Enabled = MenuStripSaveAs.Enabled = true;
                }

            }
            catch
            {

            }
            finally
            {
                Show();
            }
        }

        private void ButtonSetEntryExit_Click(object sender, EventArgs e)
        {
            CheckSetEntryExit();
        }

        private void CheckSetEntryExit()
        {
            if (_maze.IsSetExitEntry())
            {
                GBAlgGen.Enabled =
                    MenuStripSaveAs.Enabled = true;
                GroupBoxArrange.Enabled = false;
                _maze.SetWallWithExitEntry();
            }
            UpdateMaze();
        }

        private void ButtonFindWay_Click(object sender, EventArgs e)
        {
            _maze.ClearWay();
            UpdateMaze();

            new Thread(() =>
            {
                if (RBFindWayOneHand.Checked)
                {
                    SetButtonEnabled(false);
                    _maze.FindWayOneHand(UpdateMazeComponent, ref _speedAnimationFindWay);
                    SetButtonEnabled(true);
                }

                if (RBFindWayWave.Checked)
                {
                    SetButtonEnabled(false);
                    _maze.FindWayWave(UpdateMazeComponent, ref _speedAnimationFindWay);
                    SetButtonEnabled(true);
                }

            }).Start();

            if (RBFindWayManual.Checked)
            {
                _isManualFindWay = true;
            }

        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }


        private void RBFindWayManual_CheckedChanged(object sender, EventArgs e)
        {
            _maze.ClearWay();
            UpdateMaze();

            if (RBFindWayManual.Checked)
            {
                GroupBoxSpeedFindWay.Visible = false;
                GBFindWay.Size = new Size(140, 115);
                ButtonFindWay.Location = new Point(34, 86);

            }
            else
            {
                _isManualFindWay = false;
                _maze.ClearWay();
                UpdateMaze();
                GroupBoxSpeedFindWay.Visible = true;
                GroupBoxSpeedFindWay.Location = new Point(6, 86);
                GBFindWay.Size = new Size(140, 187);
                ButtonFindWay.Location = new Point(34, 158);
            }
        }

        private void RBManualGenerateEntryExit_CheckedChanged(object sender, EventArgs e)
        {
            _maze.ClearWay();
            UpdateMaze();

            if (!RBManualGenerateEntryExit.Checked)
            {
                GBArrangePattern.Visible = false;
                GBArrangePattern.Location = new Point(9, 93);
                GroupBoxArrange.Size = new Size(146, 93);
                ButtonArrangeExitEntry.Enabled = true;
            }
            else
            {
                GroupBoxArrange.Size = new Size(146, 198);
                GBArrangePattern.Visible = true;
                ButtonArrangeExitEntry.Enabled = false;
            }
            GBAlgGen.Location = new Point(14, GroupBoxArrange.Location.Y + GroupBoxArrange.Height + 5);
            GBFindWay.Location = new Point(14, GBAlgGen.Location.Y + GBAlgGen.Height + 5);
        }

        private void ButtonArrangeExitEntry_Click_1(object sender, EventArgs e)
        {
            _maze.AutoSetEntryExit();
            int sp = 40;
            UpdateMazeComponent(_maze.GetMaze(), ref sp);
            CheckSetEntryExit();
        }

        private void ComboBoxTheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_mazePictureBox != null)
                UpdateMaze();

            if (_maze != null && _dictionaryTheme != null)
            {
                _maze.Theme = _dictionaryTheme[ComboBoxTheme.Text];
                _theme = ComboBoxTheme.Text;
            }

            UpdatePictureBoxGroupBox();

        }

        private void UpdatePictureBoxGroupBox()
        {
            if (_imageMazeComponent != null)
            {
                var components = _imageMazeComponent[_dictionaryTheme[ComboBoxTheme.Text]];
                PictureBoxExit.Image = components[MazeComponent.Exit];
                PictureBoxPass.Image = components[MazeComponent.Pass];
                PictureBoxWall.Image = components[MazeComponent.Wall];
                PictureBoxEntry.Image = components[MazeComponent.Entry];
                //   Update();
            }
        }

        private void RBAlgGenAutomatic_CheckedChanged(object sender, EventArgs e)
        {
            _maze.ClearWay();
            UpdateMaze();

            if (!RBAlgGenManual.Checked)
            {
                GBALgGenSpeed.Visible = true;
                GBPatternManualGen.Visible = false;
                GBAlgGen.Size = new Size(145, 217);
            }
            else
            {
                GBALgGenSpeed.Visible = false;
                GBPatternManualGen.Visible = true;

                GBAlgGen.Size = new Size(145, 194);
            }
            GBFindWay.Location = new Point(14, GBAlgGen.Location.Y + GBAlgGen.Height + 5);
        }

        private void MenuStripSaveAs_Click(object sender, EventArgs e)
        {
            _maze.Id = 0;
            _modelMazeStruct.AddOrEdit(_maze);
        }

        private void MenuStripSave_Click(object sender, EventArgs e)
        {
            _modelMazeStruct.AddOrEdit(_maze);
        }

        private void StripMenuDevelopers_Click(object sender, EventArgs e)
        {
            new Developers().ShowDialog();
        }

        private void РуководствоПользователяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var wayHelp = $"{Directory.GetCurrentDirectory()}\\Resource\\Help\\help.html";
            if (File.Exists(wayHelp))
            {
                try
                {
                    Process.Start(wayHelp);
                }
                catch
                {
                    MessageBox.Show("Ошибка\nПроизошла ошибка во время открытия", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Ошибка\nФайл справки не найден", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void TrackBarAlgGenSpeed_ValueChanged(object sender, EventArgs e)
        {
            _speedAnimationAlgGen = 30 - 10 * (TrackBarAlgGenSpeed.Value + 1);
        }

        private int _speedAnimationFindWay;
        private void TrackBarFindWay_ValueChanged(object sender, EventArgs e)
        {
            var koefSpeed = 65;
            _speedAnimationFindWay = 200 - koefSpeed * (TrackBarFindWaySpeed.Value + 1);
        }

        public void SetButtonEnabled(bool enabled)
        {
            ButtonGenerate.Invoke(new MethodInvoker(() => ButtonGenerate.Enabled = enabled));
            ButtonGenMaze.Invoke(new MethodInvoker(() => ButtonGenMaze.Enabled = enabled));
            ButtonFindWay.Invoke(new MethodInvoker(() => ButtonFindWay.Enabled = enabled));
            menuStrip1.Invoke(new MethodInvoker(() => DBMenuToolStrip.Enabled = enabled));
            RBAlgGenPrima.Invoke(new MethodInvoker(() => RBAlgGenPrima.Enabled = enabled));
            RBAlgGenKruskala.Invoke(new MethodInvoker(() => RBAlgGenKruskala.Enabled = enabled));
            RBFindWayWave.Invoke(new MethodInvoker(() => RBFindWayWave.Enabled = enabled));
            RBFindWayOneHand.Invoke(new MethodInvoker(() => RBFindWayOneHand.Enabled = enabled));
            RBFindWayManual.Invoke(new MethodInvoker(() => RBFindWayManual.Enabled = enabled));
            RBAlgGenAutomatic.Invoke(new MethodInvoker(() => RBAlgGenAutomatic.Enabled = enabled));
            RBAlgGenManual.Invoke(new MethodInvoker(() => RBAlgGenManual.Enabled = enabled));
        }

        private void RBAutomaticGenerateEntryExit_Click(object sender, EventArgs e)
        {
            _maze.ClearWay();
            UpdateMaze();
        }
    }
}

