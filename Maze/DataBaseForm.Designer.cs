﻿namespace MazeProject
{
    partial class DataBaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DGVMaze = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lenght = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Width = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Theme = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GBMaze = new System.Windows.Forms.GroupBox();
            this.ButtonClose = new System.Windows.Forms.Button();
            this.ButtonDel = new System.Windows.Forms.Button();
            this.ButtonLoad = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DGVMaze)).BeginInit();
            this.SuspendLayout();
            // 
            // DGVMaze
            // 
            this.DGVMaze.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVMaze.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Lenght,
            this.Width,
            this.Theme});
            this.DGVMaze.Location = new System.Drawing.Point(12, 12);
            this.DGVMaze.MultiSelect = false;
            this.DGVMaze.Name = "DGVMaze";
            this.DGVMaze.ReadOnly = true;
            this.DGVMaze.Size = new System.Drawing.Size(585, 561);
            this.DGVMaze.TabIndex = 0;
            this.DGVMaze.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVMaze_RowEnter);
            // 
            // Id
            // 
            this.Id.HeaderText = "Ид";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // Lenght
            // 
            this.Lenght.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Lenght.HeaderText = "Длина";
            this.Lenght.Name = "Lenght";
            this.Lenght.ReadOnly = true;
            // 
            // Width
            // 
            this.Width.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Width.HeaderText = "Ширина";
            this.Width.Name = "Width";
            this.Width.ReadOnly = true;
            // 
            // Theme
            // 
            this.Theme.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Theme.HeaderText = "Тема";
            this.Theme.Name = "Theme";
            this.Theme.ReadOnly = true;
            // 
            // GBMaze
            // 
            this.GBMaze.Location = new System.Drawing.Point(604, 13);
            this.GBMaze.Name = "GBMaze";
            this.GBMaze.Size = new System.Drawing.Size(560, 560);
            this.GBMaze.TabIndex = 1;
            this.GBMaze.TabStop = false;
            this.GBMaze.Text = " Лабиринт";
            // 
            // ButtonClose
            // 
            this.ButtonClose.Location = new System.Drawing.Point(1100, 578);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(64, 23);
            this.ButtonClose.TabIndex = 2;
            this.ButtonClose.Text = "Выход";
            this.ButtonClose.UseVisualStyleBackColor = true;
            this.ButtonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // ButtonDel
            // 
            this.ButtonDel.Location = new System.Drawing.Point(1019, 578);
            this.ButtonDel.Name = "ButtonDel";
            this.ButtonDel.Size = new System.Drawing.Size(64, 23);
            this.ButtonDel.TabIndex = 3;
            this.ButtonDel.Text = "Удалить";
            this.ButtonDel.UseVisualStyleBackColor = true;
            this.ButtonDel.Click += new System.EventHandler(this.ButtonDel_Click);
            // 
            // ButtonLoad
            // 
            this.ButtonLoad.Location = new System.Drawing.Point(938, 577);
            this.ButtonLoad.Name = "ButtonLoad";
            this.ButtonLoad.Size = new System.Drawing.Size(64, 23);
            this.ButtonLoad.TabIndex = 4;
            this.ButtonLoad.Text = "Выбрать";
            this.ButtonLoad.UseVisualStyleBackColor = true;
            this.ButtonLoad.Click += new System.EventHandler(this.ButtonLoad_Click);
            // 
            // DataBaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1172, 613);
            this.Controls.Add(this.ButtonLoad);
            this.Controls.Add(this.ButtonDel);
            this.Controls.Add(this.ButtonClose);
            this.Controls.Add(this.GBMaze);
            this.Controls.Add(this.DGVMaze);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DataBaseForm";
            this.Text = "База данных";
            ((System.ComponentModel.ISupportInitialize)(this.DGVMaze)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DGVMaze;
        private System.Windows.Forms.GroupBox GBMaze;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lenght;
        private System.Windows.Forms.DataGridViewTextBoxColumn Width;
        private System.Windows.Forms.DataGridViewTextBoxColumn Theme;
        private System.Windows.Forms.Button ButtonClose;
        private System.Windows.Forms.Button ButtonDel;
        private System.Windows.Forms.Button ButtonLoad;
    }
}