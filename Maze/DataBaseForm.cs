﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MazeLibrary.MazeClass;
using System.Windows.Forms;
using MazeLibrary.Database;
using MazeLibrary.Enums;

namespace MazeProject
{
    public partial class DataBaseForm : Form
    {
        private Dictionary<Themes, string> _themes;
        private ModelMazeStruct _modelMazeStruct;
        private List<MazeStruct> _listMaze;
        private Dictionary<Themes, Dictionary<MazeComponent, Image>> _images;
        private long _currentIdMaze;
        public MazeStruct ChoiceMaze;


        public DataBaseForm(ModelMazeStruct modelMazeStruct, Dictionary<Themes, Dictionary<MazeComponent, Image>> imagse)
        {
            InitializeComponent();

            this.DoubleBuffered = true;

            _images = imagse;

            _modelMazeStruct = modelMazeStruct;

            _themes = new Dictionary<Themes, string>()
            {
                { Themes.Autumn, "Осень"},
                { Themes.Spring, "Весна"},
                { Themes.Summer, "Лето"},
                { Themes.Winter, "Зима"}
            };

            UpdateDGV();
        }

        private void UpdateDGV()
        {
            try
            {
                _listMaze = _modelMazeStruct.GetAll();
                _listMaze.ForEach((el) =>
                {
                    DGVMaze.Rows.Add(el.Id, el.Length, el.Width, _themes[el.Theme]);
                });
            }
            catch
            {
                Close();
            }
        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DrawMaze(MazeStruct mazeStruct)
        {
            this.Update();

            var maze = mazeStruct.GetMaze();

            // начальные
            int top = 20;
            int left = 20;
            var size = new Size()
            {
                Width = (GBMaze.Width - left * 2 - mazeStruct.Length - 1) / mazeStruct.Length,
                Height = (GBMaze.Height - top * 2 - mazeStruct.Width - 1) / mazeStruct.Width
            };

            if (size.Width > size.Height)
            {
                size.Width = size.Height;
            }
            else
            {
                size.Height = size.Width;
            }

            left = (GBMaze.Width - size.Width * mazeStruct.Length - mazeStruct.Length - 1) / 2;
            top = (GBMaze.Height - size.Height * mazeStruct.Width - mazeStruct.Width - 1) / 2;

            GBMaze.Controls.Clear();

            int countX = 0;

            for (int i = 0; i < mazeStruct.Width; i++)
            {
                int countY = 0;
                for (int j = 0; j < mazeStruct.Length; j++)
                {
                    GBMaze.Controls.Add(new PictureBox()
                    {
                        Image = _images[mazeStruct.Theme][maze[i, j]],
                        Location = new Point(left + countY + size.Width * j, top + countX + size.Height * i),
                        Size = size
                    });

                    GBMaze.Update();

                    countY++;
                }
                countX++;
            }
        }

        bool switcher = false;
        private void DGVMaze_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var element = (DataGridView)sender;
                _currentIdMaze = (long)element[0, e.RowIndex].Value;
                if (switcher)
                    DrawMaze(_listMaze.Where(obj => obj.Id == _currentIdMaze).ToList()[0]);
                else
                    switcher = !switcher;
            }
            catch { }
        }

        private void ButtonDel_Click(object sender, EventArgs e)
        {
            _modelMazeStruct.Del(_currentIdMaze);
            UpdateDGV();
        }

        private void ButtonLoad_Click(object sender, EventArgs e)
        {
            ChoiceMaze = _listMaze.Where(obj => obj.Id == _currentIdMaze).ToArray()[0];
            this.DialogResult = DialogResult.OK;
        }
    }
}
